﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NumeriX.Pro;

// Basic classes for Instrument object. 
namespace YumPAMP_Insrtument
{
    // Bond instrument object
    class BondInst
    {
        public BondInst(string object_id,
                  string ccy,
                  DateTime maturity,
                  double coupon,                  
                  string conv,
                  DateTime accrual_start,
                  double notional,
                  DateTime first_coup_date,
                  DateTime penultimate,
                  Application app,
                  ApplicationWarning warning,
                  string freq = "",
                  string basis = "",
                  string data_id = "",
                  string notice_period = "",
                  bool include_coup_dates = false,
                  double recovery = 0,
                  double quote_price = 0,
                  bool dirty_quote = false,
                  string value_date = "",
                  bool has_penultimate = true
            )

        {
            ApplicationCall call = new ApplicationCall();

            call.AddValue("ID", object_id);
            call.AddValue("OBJECT", "INSTRUMENT");
            call.AddValue("TYPE", "BOND");
            call.AddValue("CURRENCY", ccy);
            call.AddValue("MATURITY", maturity);
            call.AddValue("COUPON RATE", coupon);
            call.AddValue("CONVENTION", conv);
            call.AddValue("ACCRUAL START", accrual_start);
            call.AddValue("NOTIONAL", notional);


            // Three different parse check to build bond instrument
            // Set up new expected maturity based bonds + input market price           
            if (string.IsNullOrEmpty(data_id))
            {
                if (has_penultimate)
                {
                    call.AddValue("RECOVERY", recovery);
                    call.AddValue("QUOTE PRICE", quote_price);
                    call.AddValue("DIRTY QUOTE", dirty_quote);
                    call.AddValue("VALUE DATE", value_date);
                    call.AddValue("FREQ", freq);
                    call.AddValue("BASIS", basis);
                    call.AddValue("FIRST COUPON DATE", first_coup_date);
                    call.AddValue("PENULTIMATE COUPON DATE", penultimate);
                }
                else
                {
                    call.AddValue("RECOVERY", recovery);
                    call.AddValue("QUOTE PRICE", quote_price);
                    call.AddValue("DIRTY QUOTE", dirty_quote);
                    call.AddValue("VALUE DATE", value_date);
                    call.AddValue("FREQ", freq);
                }
            }
            // Set up callable bonds
            else if (string.IsNullOrEmpty(freq))
            {
                call.AddValue("RECOVERY", recovery);
                call.AddValue("CALL DATA", data_id);
                call.AddValue("CALL NOTICE PERIOD", notice_period);
                call.AddValue("INCLUDE COUPON DATES IN CALL DATA", include_coup_dates);
                call.AddValue("FIRST COUPON DATE", first_coup_date);
                call.AddValue("PENULTIMATE COUPON DATE", penultimate);
            }
            else
            {
                call.AddValue("CALL DATA", data_id);
                call.AddValue("CALL NOTICE PERIOD", notice_period);
                call.AddValue("INCLUDE COUPON DATES IN CALL DATA", include_coup_dates);
                call.AddValue("FREQ", freq);
                call.AddValue("BASIS", basis);
                call.AddValue("FIRST COUPON DATE", first_coup_date);
                call.AddValue("PENULTIMATE COUPON DATE", penultimate);
            }

            app.Call(call, warning);
            // Check for errors
            if (warning.HasFatal())
            {
                throw new NumeriX.Pro.ApplicationException(warning.ToString());
            }
        }
    }

    // Swaption instrument object
    class SwaptionInst
    {
        public SwaptionInst(string object_id,
                  string ccy,
                  string flavor,
                  DateTime start_date,
                  string end_tenor,
                  string strike,
                  string sigma1,
                  string index_curve,
                  string conv,
                  Application app,
                  ApplicationWarning warning
            )

        {
            ApplicationCall call = new ApplicationCall();

            call.AddValue("ID", object_id);
            call.AddValue("OBJECT", "INSTRUMENT");
            call.AddValue("TYPE", "SWAPTION");
            call.AddValue("CURRENCY", ccy);
            call.AddValue("FLAVOR", flavor);
            call.AddValue("START DATE", start_date);
            call.AddValue("ENDTENOR", end_tenor);
            call.AddValue("STRIKE", strike);
            call.AddValue("SIGMA1", sigma1);
            call.AddValue("INDEXCURVE", index_curve);
            call.AddValue("CONVENTION", conv);

            app.Call(call, warning);
            // Check for errors
            if (warning.HasFatal())
            {
                throw new NumeriX.Pro.ApplicationException(warning.ToString());
            }
        }
    }

    // Instrument collection object
    class InstColl
    {
        public InstColl(string object_id,
                  string ccy,
                  string data_id,
                  Application app,
                  ApplicationWarning warning,
                  string inst_id = ""
            )

        {
            ApplicationCall call = new ApplicationCall();

            call.AddValue("ID", object_id);
            call.AddValue("OBJECT", "INSTRUMENT COLLECTION");
            call.AddValue("TYPE", "COMPONENTS");
            call.AddValue("CURRENCY", ccy);
            // One instrument in collection
            if (string.IsNullOrEmpty(data_id))
            {
                call.AddValue("INSTRUMENT", inst_id);
            }
            // Multiple instruments in data table
            else
            {
                call.AddValue("INSTRUMENTS", data_id);
            }
           

            app.Call(call, warning);
            // Check for errors
            if (warning.HasFatal())
            {
                throw new NumeriX.Pro.ApplicationException(warning.ToString());
            }
        }
    }

    // CDS object
    class CDSInst
    {
        public CDSInst(string object_id,
                  string ccy,
                  double recovery,
                  string end_tenor,
                  DateTime start_date,
                  bool dirty,
                  bool accrued_on_default,
                  double spread,
                  string schedule_flavor,
                  Application app,
                  ApplicationWarning warning,
                  double value = 0,
                  string effective_date = "",
                  string value_Date = "",
                  DateTime end_date = default(DateTime),
                  string conv = ""
            )

        {
            ApplicationCall call = new ApplicationCall();

            call.AddValue("ID", object_id);
            call.AddValue("OBJECT", "INSTRUMENT");
            call.AddValue("TYPE", "CDS");
            call.AddValue("CURRENCY", ccy);
            call.AddValue("RECOVERY", recovery);
            call.AddValue("STARTDATE", start_date);
            call.AddValue("ACCRUED ON DEFAULT", accrued_on_default);
            call.AddValue("SPREAD", spread);

            // There different parse check to build cds instrument  
            if (string.IsNullOrEmpty(conv))
            {
                call.AddValue("END TENOR", end_tenor);
                call.AddValue("DIRTY", dirty);
                call.AddValue("SCHEDULE FLAVOR", schedule_flavor);

                if (!(string.IsNullOrEmpty(effective_date)))
                {
                    call.AddValue("VALUE", value);
                    call.AddValue("EFFECTIVE DATE", effective_date);
                    call.AddValue("VALUE DATE", value_Date);
                }
            }
            else
            {
                call.AddValue("END DATE", end_date);
                call.AddValue("CONVENTION", conv);
            }

            app.Call(call, warning);
            // Check for errors
            if (warning.HasFatal())
            {
                throw new NumeriX.Pro.ApplicationException(warning.ToString());
            }
        }
    }
}
