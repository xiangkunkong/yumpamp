﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NumeriX.Pro;

// Basic classes for Model object. 
namespace YumPAMP_Model
{
    // IR Hull White 1 Factor model object
    class IrHw1f
    {
        public IrHw1f(string object_id,
                  DateTime now_date,
                  string ccy,
                  string inst_coll_id,
                  string solver,
                  string yield_id,
                  double lambda,
                  string sigma1_settings_id,
                  string cali_method,
                  string cali_quality_table_id,
                  string option_target,
                  Application app,
                  ApplicationWarning warning
            )

        {
            ApplicationCall call = new ApplicationCall();

            call.AddValue("ID", object_id);
            call.AddValue("OBJECT", "MODEL");
            call.AddValue("TYPE", "IR");
            call.AddValue("MODEL", "Hull White 1F");
            call.AddValue("NOW DATE", now_date);
            call.AddValue("CURRENCY", ccy);           
            call.AddValue("INSTRUMENTS", inst_coll_id);
            call.AddValue("SOLVER", solver);
            call.AddValue("DOMESTIC YIELD CURVE", yield_id);
            call.AddValue("LAMBDA1", lambda);
            call.AddValue("SIGMA1", sigma1_settings_id);
            call.AddValue("CALIBRATION METHOD", cali_method);
            call.AddValue("CALIBRATION QUALITY", cali_quality_table_id);
            call.AddValue("OPTION TARGET", option_target);

            app.Call(call, warning);
            // Check for errors
            if (warning.HasFatal())
            {
                throw new NumeriX.Pro.ApplicationException(warning.ToString());
            }
        }
    }

    // HY Model object
    class HyModel
    {
        public HyModel(string object_id,
                  string model_type,
                  DateTime now_date,
                  string ccy,
                  string cali_quality_table_id,
                  string credit_curve,
                  string ir_model,
                  string cali_method,
                  Application app,
                  ApplicationWarning warning,
                  double lambda1 = 0,
                  double sigma1 = 0,
                  double corrs = 0,
                  bool joint_calibration = false
            )

        {
            ApplicationCall call = new ApplicationCall();

            // HY CR Deterministic model
            call.AddValue("ID", object_id);
            call.AddValue("OBJECT", "MODEL");
            call.AddValue("TYPE", "HY");
            call.AddValue("MODEL", model_type);
            call.AddValue("NOW DATE", now_date);
            call.AddValue("CURRENCY", ccy);
            call.AddValue("CALIBRATION METHOD", cali_method);
            call.AddValue("CALIBRATION QUALITY", cali_quality_table_id);
            call.AddValue("CREDIT CURVE", credit_curve);
            call.AddValue("IR MODEL", ir_model);

            // HY CR BK model
            if (sigma1 != 0)
            {
                call.AddValue("LAMBDA1", lambda1);
                call.AddValue("SIGMA1", sigma1);
                call.AddValue("CORRELATIONS", corrs);
                call.AddValue("JOINT CALIBRATION", joint_calibration);
            }

            app.Call(call, warning);
            // Check for errors
            if (warning.HasFatal())
            {
                throw new NumeriX.Pro.ApplicationException(warning.ToString());
            }
        }
    }
}
