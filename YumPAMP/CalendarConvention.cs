﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NumeriX.Pro;
using System.Reflection;

namespace YumPAMP_CalendarConvention
{
    // Basic class to load calendar and convention in the calculation environment.
    // This class does not use key/value pair to build calendar and convention objects. But user could use key/value pair to build these objects if they want.
    // It shows another capibility of SDK - read data from NXT file. 
    // It is useful for data like calendars and conventions, since usually they are standard and have lots of data points.
    // After user called this function - XMLUtils.ReadNXT(), objects in NXT will be loaded in the pricing environment for later use. 
    class CalendarConvention
    {
        // load calendar and convention
        public static void Load_nx_calendar_convention(Application app, ApplicationWarning warning)
        {
            string workingDirectory = Environment.CurrentDirectory;
            string fullpath = Directory.GetParent(workingDirectory).Parent.FullName + "\\CalendarConvention.nxt";
            XMLUtils.ReadNXT(app, fullpath, warning);
        }
    }
}
