﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NumeriX.Pro;

// Basic classes for Pricer/Analytic objects. 
namespace YumPAMP_Pricer
{
    // OAS Pricer object
    class PricerOAS
    {
        public PricerOAS(string object_id,
          DateTime now_date,
          string bond,         
          string cali_quality_table_id,
          string cali_method,         
          string product_name,
          string model_id,
          string basis,
          string value_date,
          string comp_type,         
          Application app,
          ApplicationWarning warning,
          int version = 0,
          double bond_clean_price = 0,
          double tolerance = 0,
          bool calculate_oas_to_riskfree = false,
          double spread = 0
        )

        {
            ApplicationCall call = new ApplicationCall();

            call.AddValue("ID", object_id);
            call.AddValue("OBJECT", "PRICER");
            call.AddValue("TYPE", "OAS");
            call.AddValue("NOW DATE", now_date);
            call.AddValue("BOND", bond);           
            call.AddValue("QUALITY", cali_quality_table_id);
            call.AddValue("METHOD", cali_method);
            call.AddValue("PRODUCT NAME", product_name);
            call.AddValue("MODEL", model_id);
            call.AddValue("SPREAD BASIS", basis);
            call.AddValue("VALUE DATE", value_date);          
            call.AddValue("COMPOUNDING TYPE", comp_type);
            
            // Solve for expected maturity
            if (version == 0)
            {
                call.AddValue("BOND CLEAN PRICE", bond_clean_price);
                call.AddValue("SPREAD TOLERANCE", tolerance);
            }
            // Solve for OAS
            else if (version == 1)
            {
                call.AddValue("BOND CLEAN PRICE", bond_clean_price);
                call.AddValue("SPREAD TOLERANCE", tolerance);
                call.AddValue("CALCULATE OAS TO RISK-FREE", calculate_oas_to_riskfree);
            }
            // OAS pricer
            else if (version == 2)
            {
                call.AddValue("SPREAD", spread);
                call.AddValue("CALCULATE OAS TO RISK-FREE", calculate_oas_to_riskfree);
            }


            app.Call(call, warning);
            // Check for errors
            if (warning.HasFatal())
            {
                throw new NumeriX.Pro.ApplicationException(warning.ToString());
            }
        }
    }

    // ANALYTIC CDS Pricer object
    class AnalyticCDS
    {
        public AnalyticCDS(string object_id,
          DateTime now_date,
          string cds,
          string credit_curve,
          string yield_curve,
          Application app,
          ApplicationWarning warning,
          string value_date = "",
          string effective_date = "",
          string recovery_method = "",
          string recovery_freq = ""
        )

        {
            ApplicationCall call = new ApplicationCall();

            call.AddValue("ID", object_id);
            call.AddValue("OBJECT", "ANALYTIC");
            call.AddValue("TYPE", "CDS");
            call.AddValue("NOWDATE", now_date);
            call.AddValue("CDS", cds);
            call.AddValue("CREDIT CURVE", credit_curve);
            call.AddValue("DOMESTIC YIELD CURVE", yield_curve);

            // Two parse check
            if (string.IsNullOrEmpty(recovery_freq))
            {
                call.AddValue("VALUE DATE", value_date);
                call.AddValue("EFFECTIVE DATE", effective_date);
                call.AddValue("RECOVERY METHOD", recovery_method);
            }
            else
            {
                call.AddValue("RECOVERY FREQUENCY", recovery_freq);
            }

            app.Call(call, warning);
            // Check for errors
            if (warning.HasFatal())
            {
                throw new NumeriX.Pro.ApplicationException(warning.ToString());
            }
        }
    }

    // ANALYTIC Bond Pricer object
    class AnalyticBond
    {
        public AnalyticBond(string object_id,
          string bond,
          double quote_price,          
          string value_date,
          string trade_date,
          bool dirty_quote,
          Application app,
          ApplicationWarning warning,
          string yield_curve = ""
        )

        {
            ApplicationCall call = new ApplicationCall();

            call.AddValue("ID", object_id);
            call.AddValue("OBJECT", "ANALYTIC");
            call.AddValue("TYPE", "BOND");
            call.AddValue("BOND", bond);
            call.AddValue("QUOTE PRICE", quote_price);
            call.AddValue("VALUE DATE", value_date);
            call.AddValue("TRADE DATE", trade_date);
            call.AddValue("DIRTY QUOTE", dirty_quote);

            if (!(string.IsNullOrEmpty(yield_curve)))
            {
                call.AddValue("DOMESTIC YIELD CURVE", yield_curve);
            }

            app.Call(call, warning);
            // Check for errors
            if (warning.HasFatal())
            {
                throw new NumeriX.Pro.ApplicationException(warning.ToString());
            }
        }
    }
}
