﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NumeriX.Pro;

// Basic classes for Report object. 
namespace YumPAMP_Report
{
    // Yield report object
    class ReportYield
    {
        public ReportYield(string object_id,
                          string pricer,
                          double shift_amount,
                          string sensitivity_type,
                          string shift_type,
                          bool recalibrate,
                          bool parallel_shift,
                          bool instrument_quotes,
                          string now_date,
                          string comp_type,
                          Application app,
                          ApplicationWarning warning,
                          string key_maturities = ""
                    )

        {
            ApplicationCall call = new ApplicationCall();

            call.AddValue("ID", object_id);
            call.AddValue("OBJECT", "REPORT");
            call.AddValue("TYPE", "YIELD");
            call.AddValue("PRICER", pricer);
            call.AddValue("SHIFT AMOUNT", shift_amount);
            call.AddValue("SENSITIVITY TYPE", sensitivity_type);
            call.AddValue("SHIFT TYPE", shift_type);
            call.AddValue("RECALIBRATE MODEL", recalibrate);
            call.AddValue("PARALLEL SHIFT", parallel_shift);
            call.AddValue("INSTRUMENT QUOTES", instrument_quotes);
            call.AddValue("NOWDATE", now_date);
            call.AddValue("COMPOUNDING TYPE", comp_type);

            if (!(string.IsNullOrEmpty(key_maturities)))
            {
                call.AddValue("KEY MATURITIES", key_maturities);
            }

            app.Call(call, warning);
            // Check for errors
            if (warning.HasFatal())
            {
                throw new NumeriX.Pro.ApplicationException(warning.ToString());
            }
        }
    }

    // Credit report object
    class ReportCredit
    {
        public ReportCredit(string object_id,                           
                            string sensitivity_type,
                            string pricer,
                            bool recalibrate,                           
                            Application app,
                            ApplicationWarning warning,
                            double shift_amount = 0,
                            string shift_type = "",
                            bool parallel_shift = false,
                            bool instrument_quotes = false,
                            DateTime begin_default = default(DateTime)
                    )

        {
            ApplicationCall call = new ApplicationCall();

            call.AddValue("ID", object_id);
            call.AddValue("OBJECT", "REPORT");
            call.AddValue("TYPE", "CREDIT");
            call.AddValue("SENSITIVITY TYPE", sensitivity_type);
            call.AddValue("PRICER", pricer);
            call.AddValue("RECALIBRATE MODEL", recalibrate);


            if (shift_amount != 0)
            {
                call.AddValue("SHIFT AMOUNT", shift_amount);
                call.AddValue("SHIFT TYPE", shift_type);
                call.AddValue("PARALLEL SHIFT", parallel_shift);
                call.AddValue("INSTRUMENT QUOTES", instrument_quotes);
            }
            // Default risk report
            else
            {
                call.AddValue("BEGIN DEFAULT", begin_default);
            }

            app.Call(call, warning);
            // Check for errors
            if (warning.HasFatal())
            {
                throw new NumeriX.Pro.ApplicationException(warning.ToString());
            }
        }
    }
}
