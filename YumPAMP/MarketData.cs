﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NumeriX.Pro;

// Basic classes for Market Data object. 
namespace YumPAMP_MarketData
{
    // Yield object for discouting curves.
    class Yield
    {
        public Yield(string object_id,
                          string interp_method,
                          string interp_var,
                          string ccy,
                          string data_id,
                          string basis,
                          string now_date,
                          Application app,
                          ApplicationWarning warning
                    )

        {
            ApplicationCall call = new ApplicationCall();

            call.AddValue("ID", object_id);           
            call.AddValue("OBJECT", "MARKET DATA");
            call.AddValue("TYPE", "YIELD");
            call.AddValue("INTERP METHOD", interp_method);
            call.AddValue("INTERP VARIABLE", interp_var);
            call.AddValue("CURRENCY", ccy);
            call.AddValue("DATA", data_id);
            call.AddValue("BASIS", basis);
            call.AddValue("NOWDATE", now_date);

            app.Call(call, warning);
            // Check for errors
            if (warning.HasFatal())
            {
                throw new NumeriX.Pro.ApplicationException(warning.ToString());
            }
        }
    }

    // 1D Interpolation object for G spread interpolator, etc.
    class Interp1D
    {
        public Interp1D(string object_id,
                        string data_id,
                        string interp_type,
                        string basis,
                        string calendar,
                        DateTime ref_date,
                        string roll_conv,                       
                        Application app,
                        ApplicationWarning warning
                    )

        {
            ApplicationCall call = new ApplicationCall();

            call.AddValue("ID", object_id);
            call.AddValue("OBJECT", "MARKET DATA");
            call.AddValue("TYPE", "1D INTERPOLATION");
            call.AddValue("DATA", data_id);
            call.AddValue("INTERPOLATION TYPE", interp_type);           
            call.AddValue("BASIS", basis);
            call.AddValue("CALENDAR", calendar);           
            call.AddValue("REFERENCE DATE", ref_date);
            call.AddValue("ROLL CONVENTION", roll_conv);

            app.Call(call, warning);
            // Check for errors
            if (warning.HasFatal())
            {
                throw new NumeriX.Pro.ApplicationException(warning.ToString());
            }
        }
    }

    // Utility function to build discounting curves DF table.
    class Load_DFs
    {
        public Load_DFs(string object_id,
                        string header1,
                        string header2,
                        string[] dates, 
                        double[] dfs, 
                        Application app,
                        ApplicationWarning warning)
        {
            ApplicationData nxData = new ApplicationData();
            nxData.AddHeader(header1);
            nxData.AddHeader(header2);

            for (int i = 0; i < dates.Length; i++)
            {
                DateTime term = DateTime.Parse(dates[i]);

                nxData.AddValue(header1, term);
                nxData.AddValue(header2, dfs[i]);
            }

            app.Data(nxData, object_id, warning);

            // Check for errors
            if (warning.HasFatal())
            {
                throw new NumeriX.Pro.ApplicationException(warning.ToString());
            }
        }
    }

    // Swaption vol object.
    class SwaptionVol
    {
        public SwaptionVol(string object_id,
                           string interp,
                           string now_date,
                           string ccy,
                           string data_id,
                           double scaling,
                           string conv,
                           string extrap,
                           string vol_basis,
                           string quote_type,
                           Application app,
                           ApplicationWarning warning
                    )

        {
            ApplicationCall call = new ApplicationCall();

            call.AddValue("ID", object_id);
            call.AddValue("OBJECT", "MARKET DATA");
            call.AddValue("TYPE", "SWAPTION VOLATILITY");
            call.AddValue("INTERP", interp);
            call.AddValue("NOWDATE", now_date);
            call.AddValue("CURRENCY", ccy);                      
            call.AddValue("DATA", data_id);
            call.AddValue("SCALING FACTOR", scaling);
            call.AddValue("CONVENTION", conv);
            call.AddValue("EXTRAPOLATION", extrap);
            call.AddValue("VOLATILITY BASIS", vol_basis);
            call.AddValue("QUOTE TYPE", quote_type);


            app.Call(call, warning);
            // Check for errors
            if (warning.HasFatal())
            {
                throw new NumeriX.Pro.ApplicationException(warning.ToString());
            }
        }
    }


    // Utility function to build swaption vol surface matrix.
    class Load_VOLs
    {
        public Load_VOLs(string object_id,
                         string[] columns,
                         string[] rows,
                         double[] vols,
                         Application app,
                         ApplicationWarning warning)
        {
            ApplicationMatrix nxMatrix = new ApplicationMatrix();

            for (int i = 0; i < columns.Length; i++)
            {
                nxMatrix.AddColumn(columns[i]);
            }

            for (int i = 0; i < rows.Length; i++)
            {
                nxMatrix.AddRow(rows[i]);
            }

            int v = 0;
            for (int i = 0; i < rows.Length; i++)
            {
                for (int j = 0; j < columns.Length; j++)
                {
                    nxMatrix.Set(i, j, vols[v]);
                    v += 1;
                }
            }

            app.Matrix(nxMatrix, object_id, warning);

            // Check for errors
            if (warning.HasFatal())
            {
                throw new NumeriX.Pro.ApplicationException(warning.ToString());
            }
        }
    }

    // CDS Curve Builder object.
    class CDSCurve
    {
        public CDSCurve(string object_id,
                        string now_date,
                        string ccy,
                        string yield_id,
                        string pay_cal,
                        bool dirty,
                        bool accrued_on_default,
                        string basis,
                        string data_id,
                        string bad_data_recovery,
                        string interp_method,
                        string interp_var,
                        string value_date,
                        string schedule_flavor,
                        string recovery_method,
                        string effective_date,
                        Application app,
                        ApplicationWarning warning
                    )

        {
            ApplicationCall call = new ApplicationCall();

            call.AddValue("ID", object_id);
            call.AddValue("OBJECT", "MARKET DATA");
            call.AddValue("TYPE", "CREDIT CURVE BUILDER");
            call.AddValue("NOWDATE", now_date);
            call.AddValue("CURRENCY", ccy);
            call.AddValue("YIELD", yield_id);
            call.AddValue("PAY CALENDAR", pay_cal);
            call.AddValue("DIRTY", dirty);
            call.AddValue("ACCRUED ON DEFAULT", accrued_on_default);
            call.AddValue("BASIS", basis);
            call.AddValue("CREDIT DATA", data_id);
            call.AddValue("BAD DATA RECOVERY", bad_data_recovery);
            call.AddValue("INTERP METHOD", interp_method);
            call.AddValue("INTERP VARIABLE", interp_var);
            call.AddValue("VALUE DATE", value_date);
            call.AddValue("SCHEDULE FLAVOR", schedule_flavor);
            call.AddValue("RECOVERY METHOD", recovery_method);
            call.AddValue("EFFECTIVE DATE", effective_date);


            app.Call(call, warning);
            // Check for errors
            if (warning.HasFatal())
            {
                throw new NumeriX.Pro.ApplicationException(warning.ToString());
            }
        }
    }

    // Utility function to build CDS data table.
    class Load_CDS_Data
    {
        public Load_CDS_Data(string object_id,
                        string[] headers,
                        string[] assets,
                        double[] vals,
                        Application app,
                        ApplicationWarning warning)
        {
            ApplicationData CDS_Data = new ApplicationData();
            int num_of_assets = 0;
            for (int i = 0; i < headers.Length; i++)
            {
                if (i == 0)
                {
                    CDS_Data.AddHeader(headers[i]);
                    CDS_Data.AddValues(headers[i], assets);
                }
                else
                {
                    CDS_Data.AddHeader(headers[i]);
                    CDS_Data.AddValues(headers[i], vals.Skip(num_of_assets).Take(4).ToList());
                    num_of_assets += 4;
                }
            }
            app.Data(CDS_Data, object_id, warning);

            // Check for errors
            if (warning.HasFatal())
            {
                throw new NumeriX.Pro.ApplicationException(warning.ToString());
            }
        }
    }

    // Credit curve viewer object.
    class CreditCurveViewer
    {
        public CreditCurveViewer(string object_id,
                           string CDS_builer_id,
                           string asset_name,
                           Application app,
                           ApplicationWarning warning
                    )

        {
            ApplicationCall call = new ApplicationCall();

            call.AddValue("ID", object_id);
            call.AddValue("OBJECT", "VIEWER");
            call.AddValue("TYPE", "CREDIT CURVE VIEWER");
            call.AddValue("CREDIT CURVE BUILDER", CDS_builer_id);
            call.AddValue("ASSET NAME", asset_name);

            app.Call(call, warning);
            // Check for errors
            if (warning.HasFatal())
            {
                throw new NumeriX.Pro.ApplicationException(warning.ToString());
            }
        }
    }

    // Credit object.
    class CreditObject
    {
        public CreditObject(string object_id,
                            string interp_method,
                            string interp_var,
                            string now_date,
                            string ccy,
                            string insts,
                            string yield_id,
                            string basis,
                            string bad_data_recovery,                           
                            Application app,
                            ApplicationWarning warning,
                            string recovery_freq = "",
                            string recovery_method = ""
                    )

        {
            ApplicationCall call = new ApplicationCall();

            call.AddValue("ID", object_id);
            call.AddValue("OBJECT", "MARKET DATA");
            call.AddValue("TYPE", "CREDIT");
            call.AddValue("INTERP METHOD", interp_method);
            call.AddValue("INTERP VARIABLE", interp_var);
            call.AddValue("NOWDATE", now_date);
            call.AddValue("CURRENCY", ccy);
            call.AddValue("INSTRUMENTS", insts);
            call.AddValue("DOMESTIC YIELD CURVE", yield_id);
            call.AddValue("BASIS", basis);
            call.AddValue("BAD DATA RECOVERY", bad_data_recovery);

            if (string.IsNullOrEmpty(recovery_freq))
            {
                call.AddValue("RECOVERY METHOD", recovery_method);
            }
            else
            {
                call.AddValue("RECOVERY FREQUENCY", recovery_freq);
            }

            app.Call(call, warning);
            // Check for errors
            if (warning.HasFatal())
            {
                throw new NumeriX.Pro.ApplicationException(warning.ToString());
            }
        }
    }
}
