﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NumeriX.Pro;

// Basic classes for Events object. 
// Whenever call a class to build an object, parameters for any specific object can be defined / changed by the user.
namespace YumPAMP_Events
{
    class SingleDate
    {
        // Single date Events object.
        public SingleDate(string object_id,
                          string name,
                          DateTime now_date,
                          Application app,
                          ApplicationWarning warning
                    )

        {         
            ApplicationCall call = new ApplicationCall();

            call.AddValue("ID", object_id);
            call.AddValue("NAME", name);
            call.AddValue("OBJECT", "EVENTS");
            call.AddValue("TYPE", "SINGLE DATE");
            call.AddValue("DATES", now_date);        

            app.Call(call, warning);
            // Check for errors
            if (warning.HasFatal())
            {
                throw new NumeriX.Pro.ApplicationException(warning.ToString());
            }
        }
    }
}
