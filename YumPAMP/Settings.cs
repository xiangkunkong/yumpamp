﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NumeriX.Pro;


// Basic classes for Settings object. 
namespace YumPAMP_Settings
{
    class CurveCali
    {
        public CurveCali(string object_id,
                         bool joint_calibration,
                         bool calibrate,
                         string calibration_type,
                         Application app,
                         ApplicationWarning warning
                        )

        {
            ApplicationCall call = new ApplicationCall();

            call.AddValue("ID", object_id);
            call.AddValue("OBJECT", "SETTINGS");
            call.AddValue("TYPE", "CURVE CALIBRATION");
            call.AddValue("JOINT CALIBRATION", joint_calibration);
            call.AddValue("CALIBRATE", calibrate);
            call.AddValue("CALIBRATION TYPE", calibration_type);

            app.Call(call, warning);
            // Check for errors
            if (warning.HasFatal())
            {
                throw new NumeriX.Pro.ApplicationException(warning.ToString());
            }
        }
    }
}
