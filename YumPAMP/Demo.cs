using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NumeriX.Pro;
using NumeriX.Pro.Analytics;
using NumeriX.Pro.MarketInformation;
using NumeriX.Pro.Instruments;
using NumeriX.Pro.Models;
using NumeriX.Pro.Structuring;
using NumeriX.Pro.Kernel;
using System.Reflection;
using YumPAMP_CalendarConvention;
using YumPAMP_Events;
using YumPAMP_DemoData;
using YumPAMP_MarketData;
using YumPAMP_Insrtument;
using YumPAMP_Model;
using YumPAMP_Settings;
using YumPAMP_Pricer;
using YumPAMP_Report;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// This is the main class to run.
// The workflow of this class is the same as the demo workbook - building tabs from left to right.
// The long comment signals mark different tabs.
// For various Numerix objects, there's a parent class. Please refer to each class for details.
// Demo data is stored in DemoData.cs, e.g. curve dfs, bond details, etc.
// Calendar and convention is stored in "CalendarConvention.nxt". Please refer to its class for details.
// After running this calss, there'll be output which are corresponding to each tab's output in the workbook.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace YumPAMP
{
    class Demo
    {
        Application app = new Application();
        ApplicationWarning warning = new ApplicationWarning();

        static void Main(string[] args)
        {

            Demo Yum = new Demo();
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Load Calendar and Conventions
            CalendarConvention.Load_nx_calendar_convention(Yum.app, Yum.warning);


            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Valuation Date
            DateTime TODAY_DATE = DateTime.Parse("29-May-2019");
            string TODAY_ID = "TODAY";
            SingleDate TODAY = new SingleDate(TODAY_ID, "TODAY", TODAY_DATE, Yum.app, Yum.warning);

            // T+2bd settle for Corp Bond.
            string CORP_BOND_SETTLE_DT_ID = "CORP_BOND_SETTLE_DT";
            DateTime date1 = Yum.app.AddTenor(TODAY_DATE, "2bd", "F", "NXNY", Yum.warning);
            SingleDate CORP_BOND_SETTLE_DT = new SingleDate(CORP_BOND_SETTLE_DT_ID, "TODAY", date1, Yum.app, Yum.warning);

            // CDS Effective Date
            string CDS_EFFECTIVE_DT_ID = "CDS_EFFECTIVE_DT";
            DateTime date2 = Yum.app.AddTenor(TODAY_DATE, "1d", "NONE", "NONE", Yum.warning);
            SingleDate CDS_EFFECTIVE_DT = new SingleDate(CDS_EFFECTIVE_DT_ID, "TODAY", date2, Yum.app, Yum.warning);

            // CDS Value Date
            string CDS_VALUE_DT_ID = "CDS_VALUE_DT";
            DateTime date3 = Yum.app.AddTenor(TODAY_DATE, "3bd", "F", "NONE", Yum.warning);
            SingleDate CDS_VALUE_DT = new SingleDate(CDS_VALUE_DT_ID, "TODAY", date3, Yum.app, Yum.warning);


            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Discounting Curves
            // Curve 260 from Bloomberg. THis is the ISDA curve used for stripping Survial Prob's off CDS quotes. Same curve used on CDSW in Bloomberg.
            // We can use any Rates Curve to strip the Surv Probs.
            // The details of the ISDA curve are available at the following location.
            // https://www.cdsmodel.com/cdsmodel/assets/cds-model/docs/Interest%20Rate%20Curve%20Specification%20-%20All%20Currencies%20(Updated%20November%208%202014)%20Final.pdf
            string USD_ISDA_DATA_ID = "CR.USD-ISDA-3M.STDRATES.SWAP.CURVE.DF.DATA";
            Load_DFs USD_ISDA_DATA = new Load_DFs(USD_ISDA_DATA_ID, "DATE", "DISCOUNT FACTOR",
                                                  DemoData.USD_ISDA_DATES, DemoData.USD_ISDA_DF,
                                                  Yum.app, Yum.warning);

            string USD_ISDA_ID = "CR.USD-ISDA-3M.STDRATES.SWAP.CURVE";
            Yield USD_ISDA = new Yield(USD_ISDA_ID, "LINEAR", "LOGDF", "USD",
                                       USD_ISDA_DATA_ID, "ACT/360", TODAY_ID,
                                       Yum.app, Yum.warning);

            // USD Fed Funds Curve. Curve 42 from Bloomberg. We need this curve when we calibrate the Rates model with swaptions.
            // The reason being that swaptions quoted in the market are assumed to the Fed Fund collateralized. 
            // So, the swaption needs 2 curves 1 X Fed Funds for discounting option and 1 X USD 3m Libor for calculating the ATM forward par swap rate on swaption expiry.
            string USD_OIS_DATA_ID = "FEDFUNDSOIS.OISCURVE.DF.DATA";
            Load_DFs USD_OIS_DATA = new Load_DFs(USD_OIS_DATA_ID, "DATE", "DISCOUNT FACTOR",
                                                 DemoData.USD_OIS_DATES, DemoData.USD_OIS_DF,
                                                 Yum.app, Yum.warning);

            string USD_OIS_ID = "FEDFUNDSOIS.OISCURVE";
            Yield USD_OIS = new Yield(USD_OIS_ID, "LINEAR", "LOGDF", "USD",
                                      USD_OIS_DATA_ID, "ACT/360", TODAY_ID,
                                      Yum.app, Yum.warning);

            // Curve 23 in Bloomberg. USD 3m Libor swaps are all collateralized in USD Fed Funds. 
            // So, the swap cashflows are discounted using USD Fed Funds and 3m Libor is used for projection the libor forard rates. 
            // So, we are stripping 3m libor rates curve given the USD Fed Funds curve. These DF's are assumed to have USD Fed Funds curve as a discounting curve for 3m Libor curve stripping.
            string USD_MID_DATA_ID = "USD_Curve_MID.DF.DATA";
            Load_DFs USD_MID_DATA = new Load_DFs(USD_MID_DATA_ID, "DATE", "DISCOUNT FACTOR",
                                                 DemoData.USD_MID_DATES, DemoData.USD_MID_DF,
                                                 Yum.app, Yum.warning);

            string USD_MID_ID = "USD_Curve_MID";
            Yield USD_MID = new Yield(USD_MID_ID, "LINEAR", "LOGDF", "USD",
                                       USD_MID_DATA_ID, "ACT/360", TODAY_ID,
                                       Yum.app, Yum.warning);

            // On the Run Treasury Curve. We dont have to use this curve. It is supplied in case a user wants to use Treasury curve.
            // THis is YCGT0025 Index from Bloomberg          
            string USD_TREASURY_DATA_ID = "US_TREASURY_CURVE.DF.DATA";
            Load_DFs USD_TREASURY_DATA = new Load_DFs(USD_TREASURY_DATA_ID, "DATE", "DISCOUNT FACTOR",
                                                 DemoData.USD_TREASURY_DATES, DemoData.USD_TREASURY_DF,
                                                 Yum.app, Yum.warning);

            string USD_TREASURY_ID = "US_TREASURY_CURVE";
            Yield USD_TREASURY = new Yield(USD_TREASURY_ID, "LINEAR", "LOGDF", "USD",
                                       USD_TREASURY_DATA_ID, "ACT/360", TODAY_ID,
                                       Yum.app, Yum.warning);

            // THis object helps to interpolate directly on the YTM of the On The Run (OTS) Treasury curve as shown in data table. 
            // This is a generic interpolator object which will interpolate any quantity. We calculate G Spread = YTM (Corp Bond) - YTM(Treasury). 
            // The YTM (Treasury) can be interpolated using this object instead of having to price the actual Corp Bond with Treasury curve to get its price --> YTM.
            // {ABSCISSA, ORDINATE} are header to the 1D interpolation object. Many market participants like to calculate CDS Spread = BCDS - Interpolated CDS spread on CDS curve. 
            // This is captured in cell in the "Pricer" part.
            string G_SPREAD_DATA_ID = "G_SPREAD_INERPOLATOR_DATA";
            Load_DFs G_SPREAD_DATA = new Load_DFs(G_SPREAD_DATA_ID, "ABSCISSA", "ORDINATE",
                                                  DemoData.G_SPREAD_INTERP_ABSCISSA, DemoData.G_SPREAD_INTERP_ORDINATE,
                                                  Yum.app, Yum.warning);

            string G_SPREAD_ID = "G_SPREAD_INERPOLATOR";
            Interp1D G_SPREAD = new Interp1D(G_SPREAD_ID, G_SPREAD_DATA_ID, "LINEAR", "ACT/ACT", "NXNY", TODAY_DATE, "F",
                                             Yum.app, Yum.warning);


            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // ATM Swaption Vol
            // Normal ATM Swaption vol for Rates model calibration. Given the low/-ve rate env, we will use normal vol rates model.
            // Please note that the market participants have switched over to quoting normal vols(quoted in bps).
            // As a result, Bloomberg and almost all market data vendors have switched over to Normal vols.
            // When normal vols are input, we should set Scaling Factor = 10000 AND Data Type = NormalVol.
            // Please note that we are using ATM Swaption vols. We can use ATM +/ -strike slices as well to build a full cube of rates vol
            string USD_VOL_DATA_ID = "USD_ATM_SWAPTION_VOL_DATA";
            Load_VOLs USD_VOL_DATA = new Load_VOLs(USD_VOL_DATA_ID, DemoData.VOL_COLUMNS, DemoData.VOL_ROWS, DemoData.VOL_VALS,
                                                   Yum.app, Yum.warning);

            // Normal vols are quoted in bps. If you are entering absolute bps like 53, then, please set scaling factor = 10000. 
            // Otherwise, you can directly input 0.53% as well. 
            // Scaling factor is to be used only if we are entering the vol as a double without any %. For Lognormal vol, scaling factor = 100
            string USD_VOL_ID = "USD_ATM_SWAPTION_VOL";
            SwaptionVol USD_VOL = new SwaptionVol(USD_VOL_ID, "CUBICSPLINE", TODAY_ID, "USD", USD_VOL_DATA_ID, 10000,
                                                  "NX.CONV.IR.USD-LIBOR-3M.SWAP", "FLAT EXTRAPOLATION", "ACT/365", "NORMAL VOL",
                                                  Yum.app, Yum.warning);


            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // CDS Curves
            // Build credit data tale
            // A user can pass any string for each CDS curve so long as they are unique and there are no duplicates. We follow Currency-Ticker-DebtType-DocClause combination.
            string[] CDS_Data_Headers = { "NAME", "6M", "1Y", "2Y", "3Y", "4Y", "5Y", "7Y", "10Y", "RECOVERY" };
            string USD_CDS_DATA_ID = "USD_CDS_FACTORY_DATA";
            Load_CDS_Data USD_CDS_DATA = new Load_CDS_Data(USD_CDS_DATA_ID, CDS_Data_Headers,
                                                           DemoData.CDS_DATA_ASSETS, DemoData.CDS_SPREAD_RECOVERY, Yum.app, Yum.warning);

            // In this demo, yield curve for the credit curve builder is USD ISDA curve, but the user can choose any curve they want.
            // A Credit Curve Builder can hold all CDS curve of a single currency. We can build a Credit Curve Builder object for all USD CDS curves. 
            // To add more CDS curve to strip, please expand the input in CREDIT DATA header
            string USD_CDS_FACTORY_ID = "USD_CDS_FACTORY";
            CDSCurve USD_CDS_FACTORY = new CDSCurve(USD_CDS_FACTORY_ID, TODAY_ID, "USD", USD_ISDA_ID, "NONE", false, true, "ACT/360",
                                                    USD_CDS_DATA_ID, "BOUND", "LINEAR", "LOGSP", CDS_VALUE_DT_ID, "CDS INDEX ROLL", "EXACT",
                                                    CDS_EFFECTIVE_DT_ID, Yum.app, Yum.warning);

            // Credit curve viewer
            string CR_USD_TSLA_SNRFOR_XR14_SP_Viewer_ID = "CR.USD-TSLA-SNRFOR-XR14_SP_CURVE";
            CreditCurveViewer CR_USD_TSLA_SNRFOR_XR14_SP_Viewer = new CreditCurveViewer(CR_USD_TSLA_SNRFOR_XR14_SP_Viewer_ID,
                                                                                        USD_CDS_FACTORY_ID, "CR.USD-TSLA-SNRFOR-XR14", Yum.app, Yum.warning);
            string CR_USD_YUM_SNRFOR_XR14_SP_Viewer_ID = "CR.USD-YUM-SNRFOR-XR14_SP_CURVE";
            CreditCurveViewer CR_USD_YUM_SNRFOR_XR14_SP_Viewer = new CreditCurveViewer(CR_USD_YUM_SNRFOR_XR14_SP_Viewer_ID,
                                                                                       USD_CDS_FACTORY_ID, "CR.USD-YUM-SNRFOR-XR14", Yum.app, Yum.warning);
            string CR_USD_FCX_SNRFOR_XR14_SP_Viewer_ID = "CR.USD-FCX-SNRFOR-XR14_SP_CURVE";
            CreditCurveViewer CR_USD_FCX_SNRFOR_XR14_SP_Viewer = new CreditCurveViewer(CR_USD_FCX_SNRFOR_XR14_SP_Viewer_ID,
                                                                                       USD_CDS_FACTORY_ID, "CR.USD-FCX-SNRFOR-XR14", Yum.app, Yum.warning);
            string CR_USD_OLN_SNRFOR_XR14_SP_Viewer_ID = "CR.USD-OLN-SNRFOR-XR14_SP_CURVE";
            CreditCurveViewer CR_USD_OLN_SNRFOR_XR14_SP_Viewer = new CreditCurveViewer(CR_USD_OLN_SNRFOR_XR14_SP_Viewer_ID,
                                                                                       USD_CDS_FACTORY_ID, "CR.USD-OLN-SNRFOR-XR14", Yum.app, Yum.warning);

            // Get SP curve ID from vredit curve viewer. We pass these SP Curve ID's for pricing downstream.
            string[] output_header1 = { "CREDIT" };
            ApplicationData output_TSLA = Yum.app.View(CR_USD_TSLA_SNRFOR_XR14_SP_Viewer_ID, output_header1, Yum.warning);
            string CR_USD_TSLA_SNRFOR_XR14_SP_ID = output_TSLA.Data(output_header1[0])[0].ToString();

            ApplicationData output_YUM = Yum.app.View(CR_USD_YUM_SNRFOR_XR14_SP_Viewer_ID, output_header1, Yum.warning);
            string CR_USD_YUM_SNRFOR_XR14_SP_ID = output_YUM.Data(output_header1[0])[0].ToString();

            ApplicationData output_FCX = Yum.app.View(CR_USD_FCX_SNRFOR_XR14_SP_Viewer_ID, output_header1, Yum.warning);
            string CR_USD_FCX_SNRFOR_XR14_SP_ID = output_FCX.Data(output_header1[0])[0].ToString();

            ApplicationData output_OLN = Yum.app.View(CR_USD_OLN_SNRFOR_XR14_SP_Viewer_ID, output_header1, Yum.warning);
            string CR_USD_OLN_SNRFOR_XR14_SP_ID = output_OLN.Data(output_header1[0])[0].ToString();

            // Build YUM CDS interplator.
            // Read dates from existing credit curve object.
            string[] output_header2 = { "DATE" };
            ApplicationData output_DATE = Yum.app.View(CR_USD_YUM_SNRFOR_XR14_SP_ID, output_header2, Yum.warning);

            // YUM CDS Interpolator Data object.
            ApplicationData YUM_CDS_INTERP_DATA = new ApplicationData();
            YUM_CDS_INTERP_DATA.AddHeader("ABSCISSA");
            YUM_CDS_INTERP_DATA.AddHeader("ORDINATE");
            string YUM_CDS_INTERP_DATA_ID = "CR.USD-YUM-SNRFOR-XR14_CDS_INTERP_DATA";
            int temp = 1;

            for (int i = 0; i < output_DATE.Data("DATE").Count; i++)
            {
                DateTime term = DateTime.Parse(output_DATE.Data("DATE")[i].ToString());
                YUM_CDS_INTERP_DATA.AddValue("ABSCISSA", term);

                YUM_CDS_INTERP_DATA.AddValue("ORDINATE", DemoData.CDS_SPREAD_RECOVERY[temp]);
                temp += 4;
            }
            Yum.app.Data(YUM_CDS_INTERP_DATA, YUM_CDS_INTERP_DATA_ID, Yum.warning);

            // YUM CDS interplator.
            string YUM_CDS_INTERP_ID = "CR.USD-YUM-SNRFOR-XR14_CDS_INTERP";
            Interp1D YUM_CDS_INTERP = new Interp1D(YUM_CDS_INTERP_ID, YUM_CDS_INTERP_DATA_ID, "LINEAR", "ACT/360", "NXNY", TODAY_DATE, "F",
                                                   Yum.app, Yum.warning);


            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Callable Bond SP Curve stripping
            // STEP 1: SET UP CALLABLE BONDS TO SOLVE FOR EXPECTED MATURITY WITH RATES MODEL ONLY
            // Custom Bermudan call schedule. A user can add more call dates as needed.
            for (int i = 0; i < DemoData.CALLABLE_BONDS.Length; i++)
            {
                string data_id = DemoData.CALLABLE_BONDS[i] + "_DATA";
                ApplicationData nxData = new ApplicationData();
                nxData.AddHeader("DATE");
                nxData.AddHeader("VALUE");
                if (i != DemoData.CALLABLE_BONDS.Length - 1)
                {
                    // In this demo, for first 3 bonds, there're 3 call dates, the only call date associated with the bond is the 1st call date
                    DateTime term = new DateTime();
                    for (int j = 0; j < DemoData.CALLABLE_BONDS.Length - 1; j++)
                    {
                        if (j == 0)
                        {
                            term = DateTime.Parse(DemoData.CALLABLE_BONDS_DATES[i, 0]);
                        }
                        else
                        {
                            term = Yum.app.AddTenor(term, "1m", "NONE", "NONE", Yum.warning);
                        }

                        nxData.AddValue("DATE", term);
                        nxData.AddValue("VALUE", 100);
                    }
                }
                else
                {
                    // In this demo, for 4th bond, there're 4 call dates, the only call date associated with the bond is the 1st call date
                    DateTime term = new DateTime();
                    for (int j = 0; j < DemoData.CALLABLE_BONDS.Length; j++)
                    {
                        if (j == 0)
                        {
                            term = DateTime.Parse(DemoData.CALLABLE_BONDS_DATES[i, 0]);
                        }
                        else
                        {
                            term = Yum.app.AddTenor(term, "1m", "NONE", "NONE", Yum.warning);
                        }

                        nxData.AddValue("DATE", term);
                        nxData.AddValue("VALUE", 100);
                    }
                }

                Yum.app.Data(nxData, data_id, Yum.warning);
            }

            // Bond instrument
            for (int i = 0; i < DemoData.CALLABLE_BONDS.Length; i++)
            {
                string object_id = DemoData.CALLABLE_BONDS[i];
                string data_id = DemoData.CALLABLE_BONDS[i] + "_DATA";
                double bond_coup_rate = DemoData.CALLABLE_BONDS_COUPON[i];
                DateTime bond_maturity = DateTime.Parse(DemoData.CALLABLE_BONDS_DATES[i, 1]);
                DateTime bond_accrual_start = DateTime.Parse(DemoData.CALLABLE_BONDS_DATES[i, 2]);
                DateTime bond_first_coup = DateTime.Parse(DemoData.CALLABLE_BONDS_DATES[i, 3]);
                DateTime bond_penultimate = DateTime.Parse(DemoData.CALLABLE_BONDS_DATES[i, 4]);

                BondInst object_bond = new BondInst(object_id, "USD", bond_maturity,
                                                    bond_coup_rate, "NX.CONV.IR.BOND.US.CORPORATE",
                                                    bond_accrual_start, 100, bond_first_coup, bond_penultimate,
                                                    Yum.app, Yum.warning, "6M", "30/360", data_id, "30D", true);
            }

            // STEP 2: SET UP RATES MODEL
            // Swaption instruments to calibrate IR Model
            // Diagonal Swaption calibration setup. The YUM callable bonds are callable only during the last coupon period
            // They have a make whole provision that has been ignored for calculating Expected Maturity 
            for (int i = 0; i < DemoData.DIAGONAL_SWAPTION_TENOR.GetLength(1); i++)
            {
                string option_tenor = DemoData.DIAGONAL_SWAPTION_TENOR[0, i];
                string swap_tenor = DemoData.DIAGONAL_SWAPTION_TENOR[1, i];
                string object_id = option_tenor + "_" + swap_tenor + "_SWAPTION";
                DateTime term = Yum.app.AddTenor(TODAY_DATE, option_tenor, "F", "NXNY", Yum.warning);

                SwaptionInst object_swaption = new SwaptionInst(object_id, "USD", "PAYERS", term, DemoData.DIAGONAL_SWAPTION_TENOR[1, i], "ATM",
                                                                USD_VOL_ID, USD_MID_ID, "NX.CONV.IR.USD-LIBOR-3M.SWAP",
                                                                Yum.app, Yum.warning);
            }

            // Data object to store all swaptions id
            string INST_COLL_DATA_ID = "DIAGONAL_SWAPTION_COLLECTION_DATA";

            ApplicationData InstCollData = new ApplicationData();
            InstCollData.AddHeader("ID");
            for (int i = 0; i < DemoData.DIAGONAL_SWAPTION_TENOR.GetLength(1); i++)
            {
                string option_tenor = DemoData.DIAGONAL_SWAPTION_TENOR[0, i];
                string swap_tenor = DemoData.DIAGONAL_SWAPTION_TENOR[1, i];
                string object_id = option_tenor + "_" + swap_tenor + "_SWAPTION";
                InstCollData.AddValue("ID", object_id);
            }
            Yum.app.Data(InstCollData, INST_COLL_DATA_ID, Yum.warning);

            // Instrument collection object
            string INST_COLL_ID = "DIAGONAL_SWAPTION_COLLECTION";
            InstColl DIAGONAL_SWAPTION_COLLECTION = new InstColl(INST_COLL_ID, "USD", INST_COLL_DATA_ID, Yum.app, Yum.warning);


            // Sigma1 curve calibration settings object
            string MODEL_SIGMA_SETTING_ID = "MODEL_SIGMA_SETTING";
            CurveCali MODEL_SIGMA_SETTING = new CurveCali(MODEL_SIGMA_SETTING_ID, false, true, "STEP", Yum.app, Yum.warning);

            // Calibration quality data object
            string CALI_QUALITY_ID = "FD_CALIB_QUALITY";

            ApplicationData CaliQuaData = new ApplicationData();
            CaliQuaData.AddHeader("TIMESTEPS");
            CaliQuaData.AddHeader("XSTEPS");
            CaliQuaData.AddHeader("YSTEPS");
            CaliQuaData.AddValue("TIMESTEPS", 100);
            CaliQuaData.AddValue("XSTEPS", 100);
            CaliQuaData.AddValue("YSTEPS", 100);
            Yum.app.Data(CaliQuaData, CALI_QUALITY_ID, Yum.warning);

            // IR HW1F model object
            // The Rates model build to convert the callable bond into its bullet equivalent by solving for expected call date/maturity
            // Once, we have the expected maturity, we build bullet bonds with same input market price and solve for survival prob
            string HW1F_EXPECTED_MATURITY_ID = "YUM_IR_Hull White 1F_EXPECTEDMATURITYMODEL";
            IrHw1f HW1F_EXPECTED_MATURITY = new IrHw1f(HW1F_EXPECTED_MATURITY_ID, TODAY_DATE, "USD", INST_COLL_ID, "FAST",
                                                       USD_OIS_ID, 0.03, MODEL_SIGMA_SETTING_ID, "BackwardFinDiff",
                                                       CALI_QUALITY_ID, "TARGET PRICE", Yum.app, Yum.warning);

            // STEP 3: SOLVE FOR EXPECTED MATURITY USING RATES MODEL + INPUT PRICE
            // OAS Pricer object
            // List to store pricer output expected maturity
            List<DateTime> expected_maturity = new List<DateTime>();

            // Output format
            Console.WriteLine(new string('=', 50));
            Console.WriteLine("Callable Bond SP Curve stripping Tab Output");
            Console.WriteLine(new string('=', 50));
            Console.WriteLine("STEP 3: Callable bonds expected maturity:");

            for (int i = 0; i < DemoData.CALLABLE_BONDS_CLEAN_PRICE.Length; i++)
            {
                string object_id = DemoData.CALLABLE_BONDS[i] + "_OAS_SOLVER";
                string bond_id = DemoData.CALLABLE_BONDS[i];
                double bond_clean_p = DemoData.CALLABLE_BONDS_CLEAN_PRICE[i];
                PricerOAS object_oas = new PricerOAS(object_id, TODAY_DATE, bond_id, CALI_QUALITY_ID, "BackwardFinDiff",
                                                     "CALLABLEBOND", HW1F_EXPECTED_MATURITY_ID, "30/360", CORP_BOND_SETTLE_DT_ID,
                                                     "S", Yum.app, Yum.warning, 0, bond_clean_p, 1E-14);

                // Output each bond's expected maturity               
                string header_name = bond_id + ".EXPECTEDMATURITY";
                string[] output_header = { header_name };
                ApplicationData output_expected_maturity = Yum.app.View(object_id, output_header, Yum.warning);
                DateTime term = DateTime.Parse(output_expected_maturity.Data(header_name)[0].ToString());
                // Output expected maturity
                Console.WriteLine(header_name + "\t" + term);
                expected_maturity.Add(term);
            }
            // Output format
            Console.WriteLine("");

            // STEP 4: SET UP NEW EXPECTED MATURITY BASED BONDS + INPUT MARKET PRICE
            // Bond instrument
            // Same Recovery as CDS. Since these are Senior Unsecured, we have set it to 40%
            // List to store bond instruments id to strip SP curve
            List<string> YumSPInstColl_ids = new List<string>();

            for (int i = 0; i < DemoData.CALLABLE_BONDS.Length; i++)
            {
                string object_id = DemoData.CALLABLE_BONDS[i] + "_EXPECTEDMATURITY_BOND";
                double bond_coup_rate = DemoData.CALLABLE_BONDS_COUPON[i];
                DateTime bond_maturity = expected_maturity[i];
                DateTime bond_accrual_start = DateTime.Parse(DemoData.CALLABLE_BONDS_DATES[i, 2]);
                DateTime bond_first_coup = DateTime.Parse(DemoData.CALLABLE_BONDS_DATES[i, 3]);
                DateTime bond_penultimate = DateTime.Parse(DemoData.CALLABLE_BONDS_DATES[i, 4]);
                double bond_clean_p = DemoData.CALLABLE_BONDS_CLEAN_PRICE[i];

                BondInst object_bond = new BondInst(object_id, "USD", bond_maturity,
                                                    bond_coup_rate, "NX.CONV.IR.BOND.US.CORPORATE",
                                                    bond_accrual_start, 100, bond_first_coup, bond_penultimate,
                                                    Yum.app, Yum.warning, "6M", "30/360", "", "", false, 0.4, bond_clean_p, false, CORP_BOND_SETTLE_DT_ID);

                YumSPInstColl_ids.Add(object_id);
            }

            // Two bullet bonds instruments
            for (int i = 0; i < DemoData.BULLET_BONDS.Length; i++)
            {
                string object_id = DemoData.BULLET_BONDS[i];
                double bond_coup_rate = DemoData.BULLET_BONDS_COUPON[i];
                double bond_quote_p = DemoData.BULLET_BONDS_QUOTE_PRICE[i];
                DateTime bond_maturity = DateTime.Parse(DemoData.BULLET_BONDS_DATES[i, 0]);
                DateTime bond_accrual_start = DateTime.Parse(DemoData.BULLET_BONDS_DATES[i, 1]);
                DateTime bond_first_coup = DateTime.Parse(DemoData.BULLET_BONDS_DATES[i, 2]);
                DateTime bond_penultimate = DateTime.Parse(DemoData.BULLET_BONDS_DATES[i, 3]);

                BondInst object_bond = new BondInst(object_id, "USD", bond_maturity,
                                                    bond_coup_rate, "NX.CONV.IR.BOND.US.CORPORATE",
                                                    bond_accrual_start, 100, bond_first_coup, bond_penultimate,
                                                    Yum.app, Yum.warning, "6M", "30/360", "", "", false, 0.4, bond_quote_p, false, CORP_BOND_SETTLE_DT_ID);

                YumSPInstColl_ids.Add(object_id);
            }

            // STEP 5: STRIP THE SURVIVAL PROBABILITY CURVE FROM BONDS
            // Data object to store all bonds id
            string YUM_SP_CURVE_COLL_DATA_ID = "YUM_BONDS_BASED_SP_CURVE_COLL_DATA";

            ApplicationData YumSPInstCollData = new ApplicationData();
            YumSPInstCollData.AddHeader("ID");
            YumSPInstCollData.AddValues("ID", YumSPInstColl_ids);
            Yum.app.Data(YumSPInstCollData, YUM_SP_CURVE_COLL_DATA_ID, Yum.warning);

            // Bond instrument collection object
            string YUM_SP_CURVE_COLL_ID = "YUM_BONDS_BASED_SP_CURVE_COLL";
            InstColl YUM_SP_CURVE_COLL = new InstColl(YUM_SP_CURVE_COLL_ID, "USD", YUM_SP_CURVE_COLL_DATA_ID, Yum.app, Yum.warning);

            // Credit object to strip SP curve
            // To generate CS01/CR01, we cannot pass a Surv Prob stripped off bonds. We need to build a CDS spread based curve. 
            // These bond impled Surv Probs are converted into their CDS equivalent par spreads in steps 6 ~ 8 below
            string YUM_SP_CURVE_ID = "YUM_BONDS_BASED_SP_CURVE";

            // Frequecny is 6m, same frequency as bond coupon. This frequency sets up the default leg in bond pricing equation for SP stripping. 
            // Default Leg = Notional * Recovery * SUM(SP(i - 1) - SP(i)) * DF(i) where i = coupon payment dates
            // Bond PV = SUM[Coupon * DCF(i) * DF(i) * SP(i)] + Notional * DF(End) * SP(End) + Notional * Recovery * SUM(SP(i - 1) - SP(i)) * DF(i) where i = coupon payment dates
            // We strip SP(i) in the bootstrapper.
            CreditObject YUM_SP_CURVE = new CreditObject(YUM_SP_CURVE_ID, "LINEAR", "LOGSP", TODAY_ID, "USD", YUM_SP_CURVE_COLL_ID,
                                                         USD_OIS_ID, "30/360", "BOUND", Yum.app, Yum.warning, "6M");

            // Build output table for SP
            string[] output_header_sp = { "DATE", "SURVIVAL PROBABILITY" };
            ApplicationData output_sp = Yum.app.View(YUM_SP_CURVE_ID, output_header_sp, Yum.warning);
            output_sp.AddHeader("SURVIVAL_PROBABILITY_FROM_CDS_CURVE");

            for (int i = 0; i < output_sp.Data("DATE").Count; i++)
            {
                DateTime term = DateTime.Parse(output_sp.Data("DATE")[i].ToString());
                double cds_sp = Yum.app.GetSurvivalProbability(CR_USD_YUM_SNRFOR_XR14_SP_ID, TODAY_DATE, term, Yum.warning);
                output_sp.AddValue("SURVIVAL_PROBABILITY_FROM_CDS_CURVE", cds_sp);
            }

            // Output SP table
            Console.WriteLine("STEP 5: SP Curve from bonds and SP Curve from cds:");
            Console.WriteLine(output_sp);
            Console.WriteLine("");

            // STEP 6: SET UP SPOT STARTING CDS' TO SOLVE FOR PAR SPREAD OFF BOND IMPLED SURV PROBS FROM STEP 5
            for (int i = 1; i < CDS_Data_Headers.Length - 1; i++)
            {
                string object_id = CDS_Data_Headers[i] + "_PS_SOLVER_INST";
                string endtenor = CDS_Data_Headers[i];
                CDSInst object_cds = new CDSInst(object_id, "USD", 0.4, endtenor, TODAY_DATE, false, true, 0, "CDS INDEX ROLL", Yum.app, Yum.warning);
            }

            // STEP 7: SET UP CDS PRICER TO SOLVE FOR PAR SPREAD USING CDS' SET UP IN STEP 6
            // List to store solved cds spread
            List<double> bond_implied_cds_spread = new List<double>();

            // Output
            Console.WriteLine("STEP 7: CDS par spread implied from bonds and cds:");

            for (int i = 1; i < CDS_Data_Headers.Length - 1; i++)
            {
                string object_id = CDS_Data_Headers[i] + "_PS_SOLVER_INST_PRICER";
                string cds_id = CDS_Data_Headers[i] + "_PS_SOLVER_INST";

                // YUM_SP_CURVE is the Surv Prob curve off the Yum Bonds from step 5
                // Yield curve is USD FEDFUNDS OIS. 
                // One can use the same discounting curve used to strip the surv prob from CDS in the "CDS Curves" part for a closer "apples to apples" comparison.  
                // Alternatively, we can the use same curve used to calculated expected maturity in step 2
                AnalyticCDS analytic_cds = new AnalyticCDS(object_id, TODAY_DATE, cds_id, YUM_SP_CURVE_ID, USD_OIS_ID,
                                                           Yum.app, Yum.warning, CDS_VALUE_DT_ID, CDS_EFFECTIVE_DT_ID, "EXACT");

                // Output BOND IMPLIED CDS PAR SPREAD vs CDS PAR SPREAD
                string[] output_header_cds_analytic = { "FORWARD" };
                ApplicationData output_cds_analytic = Yum.app.View(object_id, output_header_cds_analytic, Yum.warning);

                // Index used to find CDS par spread in DemoData.CDS_SPREAD_RECOVERY data table   
                int j = i;
                j = 4 * (i - 1) + 1;
                // Output CDS SPREAD
                Console.WriteLine(object_id + "\t");
                Console.WriteLine("BOND IMPLIED CDS PAR SPREAD:  " + output_cds_analytic.Data("FORWARD")[0] + "\t");
                Console.WriteLine("CDS PAR SPREAD:               " + DemoData.CDS_SPREAD_RECOVERY[j] + "\t");

                // Add implied spread to the list for later use
                string implied_spread = output_cds_analytic.Data("FORWARD")[0].ToString();
                bond_implied_cds_spread.Add(Convert.ToDouble(implied_spread));
            }
            // Output format
            Console.WriteLine("");

            // STEP 8: SET UP NEW CDS WITH SOLVED FOR PAR SPREAD FROM STEP 7 TO RESTRIP THE SURV PROB WITH EQUIVALENT PAR CDS
            // List to store CDS instruments id to strip SP curve
            List<string> YumSPInstColl_BCDS_ids = new List<string>();
            for (int i = 1; i < CDS_Data_Headers.Length - 1; i++)
            {
                string object_id = CDS_Data_Headers[i] + "_CS_INST";
                string endtenor = CDS_Data_Headers[i];
                CDSInst object_cds = new CDSInst(object_id, "USD", 0.4, endtenor, TODAY_DATE, false, true, bond_implied_cds_spread[i - 1],
                                                "CDS INDEX ROLL", Yum.app, Yum.warning, 0, CDS_EFFECTIVE_DT_ID, CDS_VALUE_DT_ID);
                YumSPInstColl_BCDS_ids.Add(object_id);
            }

            // Instrument collection object used to strip the SP curve
            string YUM_SP_CURVE_COLL_BCDS_DATA_ID = "YUM_BCDS_SET_DATA";
            ApplicationData YumSPInstCollBCDSData = new ApplicationData();
            YumSPInstCollBCDSData.AddHeader("ID");
            YumSPInstCollBCDSData.AddValues("ID", YumSPInstColl_BCDS_ids);
            Yum.app.Data(YumSPInstCollBCDSData, YUM_SP_CURVE_COLL_BCDS_DATA_ID, Yum.warning);

            string YUM_BCDS_CURVE_INST_COLL_ID = "YUM_BCDS_CURVE_INST_COLL";
            InstColl YUM_BCDS_CURVE_INST_COLL = new InstColl(YUM_BCDS_CURVE_INST_COLL_ID, "USD", YUM_SP_CURVE_COLL_BCDS_DATA_ID, Yum.app, Yum.warning);

            // This is the Bond Implied Surv Prob curve we will use to feed into the Credit Model and this will produce CR01 as it is based on par CDS spreads.
            string YUM_BCDS_SP_CURVE_ID = "YUM_BCDS_SP_CURVE";
            CreditObject YUM_BCDS_SP_CURVE = new CreditObject(YUM_BCDS_SP_CURVE_ID, "LINEAR", "LOGSP", TODAY_ID, "USD", YUM_BCDS_CURVE_INST_COLL_ID,
                                                         USD_OIS_ID, "30/360", "BOUND", Yum.app, Yum.warning, "", "EXACT");

            // List to store YUM BCDS SP dates
            List<DateTime> bcds_sp_date = new List<DateTime>();
            string[] output_header_bcds_sp = { "DATE" };
            ApplicationData output_bcds_sp = Yum.app.View(YUM_BCDS_SP_CURVE_ID, output_header_bcds_sp, Yum.warning);
            for (int i = 0; i < output_bcds_sp.Data("DATE").Count; i++)
            {
                DateTime term = DateTime.Parse(output_bcds_sp.Data("DATE")[i].ToString());
                bcds_sp_date.Add(term);
            }

            // {ABSCISSA, ORDINATE} are header to the 1D interpolation object. 
            // Many market participants like to calculate CDS Spread = BCDS - Interpolated CDS spread on CDS curve. 
            ApplicationData YUM_BCDS_INTERP_DATA = new ApplicationData();
            YUM_BCDS_INTERP_DATA.AddHeader("ABSCISSA");
            YUM_BCDS_INTERP_DATA.AddHeader("ORDINATE");
            YUM_BCDS_INTERP_DATA.AddValues("ABSCISSA", bcds_sp_date);
            YUM_BCDS_INTERP_DATA.AddValues("ORDINATE", bond_implied_cds_spread);
            string YUM_BCDS_INTERP_DATA_ID = "YUM_BCDS_INTERP_DATA";
            Yum.app.Data(YUM_BCDS_INTERP_DATA, YUM_BCDS_INTERP_DATA_ID, Yum.warning);

            // YUM BCDS Interpolator
            string YUM_BCDS_INTERP_ID = "YUM_BCDS_INTERP";
            Interp1D YUM_BCDS_INTERP = new Interp1D(YUM_BCDS_INTERP_ID, YUM_BCDS_INTERP_DATA_ID, "LINEAR", "ACT/360", "NXNY", TODAY_DATE, "F",
                                                    Yum.app, Yum.warning);


            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Pricing Models
            // We can use the same Rates model as one used to strip surv probs off Yum bonds (including callable bonds).  
            // In this case, we have build a new Rates model where the swaptions are co terminal with call dates of US48250NAB10 bond.
            // Build swaptions
            // List to store swaptions ids
            List<string> YumSwptColl_ids = new List<string>();

            for (int i = 0; i < DemoData.US48250NAB10_DATES.Length - 1; i++)
            {
                DateTime start_date = DateTime.Parse(DemoData.US48250NAB10_DATES[i]);
                DateTime maturity = DateTime.Parse(DemoData.US48250NAB10_DATES[4]);
                double tenor_temp = Application.GetDayCountFraction("30/360", start_date, maturity, Yum.warning);
                string tenor = Convert.ToString(tenor_temp) + "Y";
                string object_id = DemoData.US48250NAB10_DATES[i] + "_SWAPTION";

                SwaptionInst object_swaption = new SwaptionInst(object_id, "USD", "PAYERS", start_date, tenor, "ATM",
                                                                USD_VOL_ID, USD_MID_ID, "NX.CONV.IR.USD-LIBOR-3M.SWAP",
                                                                Yum.app, Yum.warning);
                YumSwptColl_ids.Add(object_id);
            }

            // Data object to store all swaptions id
            string YUM_SWPT_COLL_DATA_ID = "YUM_SWPT_COLL_DATA";

            ApplicationData YumSwptCollData = new ApplicationData();
            YumSwptCollData.AddHeader("ID");
            YumSwptCollData.AddValues("ID", YumSwptColl_ids);
            Yum.app.Data(YumSwptCollData, YUM_SWPT_COLL_DATA_ID, Yum.warning);

            // Instrument collection object
            string YUM_SWPT_COLL_ID = "YUM_SWAPTION_COLL";
            InstColl YUM_SWPT_COLL = new InstColl(YUM_SWPT_COLL_ID, "USD", YUM_SWPT_COLL_DATA_ID, Yum.app, Yum.warning);

            // SWAPTION CALIBRATED RATES Hull White 1F MODEL	
            string YUM_HW1F_ID = "YUM_IR_Hull White 1F_MODEL";
            IrHw1f YUM_HW1F = new IrHw1f(YUM_HW1F_ID, TODAY_DATE, "USD", YUM_SWPT_COLL_ID, "FAST",
                                         USD_OIS_ID, 0.03, MODEL_SIGMA_SETTING_ID, "BackwardFinDiff",
                                         CALI_QUALITY_ID, "TARGET PRICE", Yum.app, Yum.warning);


            // SWAPTION CALIBRATED RATES Hull White 1F MODEL_CR DETERMINISTIC MODEL	
            string YUM_HW1F_CR_DET_ID = "YUM_IR_HULLWHITE1F_MODEL_CR DETERMINISTIC";
            HyModel YUM_HW1F_CR_DET = new HyModel(YUM_HW1F_CR_DET_ID, "CR DETERMINISTIC", TODAY_DATE, "USD", CALI_QUALITY_ID,
                                                  CR_USD_YUM_SNRFOR_XR14_SP_ID, YUM_HW1F_ID, "BackwardFinDiff", Yum.app, Yum.warning);

            // SWAPTION CALIBRATED RATES Hull White 1F MODEL_CR BK MODEL
            // Currently, only CDS are supported as calibrating instruments. 
            // SO, if we strip Surv Probs off Bonds, we need to convert em into CDS equilavent par spreads as laid out in the Step 8 in the "CallableBond SP  Curve Stripping" part. 
            // Also CR01 is calculated by bumpgin CDS spread up and down. THis cannot be done with Bond Prices. We will add support for bonds as calibrating . We need to think about CR01 for bonds
            // Joint calibration of CR model in presence of stochastic IR model. TRUE will reprice back the curve stripping CDS'

            // To be consistent with workbook, currently comment this object, user could uncomment and generate this object
            /*
            string YUM_HW1F_CR_BK_ID = "YUM_IR_HULLWHITE1F_MODEL_CR BK_MODEL";           
            HyModel YUM_HW1F_CR_BK = new HyModel(YUM_HW1F_CR_BK_ID, "CR BK", TODAY_DATE, "USD", CALI_QUALITY_ID,
                                                 CR_USD_YUM_SNRFOR_XR14_SP_ID, YUM_HW1F_ID, "BackwardFinDiff", Yum.app, Yum.warning, 
                                                 0.03, 0.5, 0, true);
            */


            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // US48250NAB10
            // Data table of bond call dates
            string US48250NAB10_BOND_CALL_DATA_ID = "US48250NAB10.BOND_CALL_DATES";
            ApplicationData US48250NAB10_BOND_CALL_DATA = new ApplicationData();
            US48250NAB10_BOND_CALL_DATA.AddHeader("DATE");
            US48250NAB10_BOND_CALL_DATA.AddHeader("VALUE");

            for (int i = 0; i < DemoData.US48250NAB10_BOND_DATES.Length; i++)
            {
                US48250NAB10_BOND_CALL_DATA.AddValue("DATE", DateTime.Parse(DemoData.US48250NAB10_DATES[i]));
                US48250NAB10_BOND_CALL_DATA.AddValue("VALUE", DemoData.US48250NAB10_BOND_PRICE[i]);
            }
            Yum.app.Data(US48250NAB10_BOND_CALL_DATA, US48250NAB10_BOND_CALL_DATA_ID, Yum.warning);

            // US48250NAB10 bond instrument
            DateTime US48250NAB10_maturity = DateTime.Parse(DemoData.US48250NAB10_BOND_DATES[0]);
            DateTime US48250NAB10_accrual_start = DateTime.Parse(DemoData.US48250NAB10_BOND_DATES[1]);
            DateTime US48250NAB10_first_coup = DateTime.Parse(DemoData.US48250NAB10_BOND_DATES[2]);
            DateTime US48250NAB10_penultimate = DateTime.Parse(DemoData.US48250NAB10_BOND_DATES[3]);

            // Recovery = 40%.
            // Bond Instrument with economic terms of the bond and the call schedule. Goes down stream into OAS solving, risk reports etc
            // The reason for calling it "risky" is we have populated the Recovery header in the bond instrument and hence it requires a CDS curve OR Credit Model.
            // If we dont populate recovery, then, the bond can be priced with Rates Curve OR Rates Model only           
            string US48250NAB10_BOND_ID = "US48250NAB10.BOND";
            BondInst US48250NAB10_BOND = new BondInst(US48250NAB10_BOND_ID, "USD", US48250NAB10_maturity,
                                                0.0525, "NX.CONV.IR.BOND.US.CORPORATE",
                                                US48250NAB10_accrual_start, 100, US48250NAB10_first_coup, US48250NAB10_penultimate,
                                                Yum.app, Yum.warning, "", "", US48250NAB10_BOND_CALL_DATA_ID, "30D", true, 0.4);

            // YTX_SOLVER
            // Yield to Call for each call date is generated and we do a MIN() to get YTW.
            string US48250NAB10_BOND_YTX_SOLVER_ID = "US48250NAB10.BOND_YTX_SOLVER";
            AnalyticBond US48250NAB10_BOND_YTX_SOLVER = new AnalyticBond(US48250NAB10_BOND_YTX_SOLVER_ID, US48250NAB10_BOND_ID, 101.419,
                                                                         CORP_BOND_SETTLE_DT_ID, TODAY_ID, false, Yum.app, Yum.warning);

            string[] output_header_bond_ytx = { "YIELD TO WORST", "WORST DATE", "YIELD TO MATURITY" };
            ApplicationData output_bond_ytx = Yum.app.View(US48250NAB10_BOND_YTX_SOLVER_ID, output_header_bond_ytx, Yum.warning);

            // Output format
            Console.WriteLine(new string('=', 50));
            Console.WriteLine("US48250NAB10 Tab Output");
            Console.WriteLine(new string('=', 50));
            Console.WriteLine("US48250NAB10.BOND_YTX_SOLVER:");
            Console.WriteLine(output_bond_ytx);
            Console.WriteLine("");

            // Z_SPRD_SOLVER
            // Z Spread is typically calculated on top of 3m libor. So, we are using 3m Libor curve
            string US48250NAB10_BOND_Z_SPREAD_SOLVER_ID = "US48250NAB10.BOND_Z_SPRD_SOLVER";
            AnalyticBond US48250NAB10_BOND_Z_SPREAD_SOLVER = new AnalyticBond(US48250NAB10_BOND_Z_SPREAD_SOLVER_ID, US48250NAB10_BOND_ID, 101.419,
                                                                         CORP_BOND_SETTLE_DT_ID, TODAY_ID, false, Yum.app, Yum.warning, USD_MID_ID);

            string[] output_header_bond_zspread = { "Z-SPREAD" };
            ApplicationData output_bond_zspread = Yum.app.View(US48250NAB10_BOND_Z_SPREAD_SOLVER_ID, output_header_bond_zspread, Yum.warning);

            // Output
            Console.WriteLine("US48250NAB10.BOND_Z_SPRD_SOLVER:");
            Console.WriteLine(output_bond_zspread);
            Console.WriteLine("");


            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // OAS Solver NO CDS
            // Pricer Calibration quality data object
            string FD_PRICING_TIME_STEPS_ID = "FD_PRICING_TIME_STEPS";

            ApplicationData FDPricingQuaData = new ApplicationData();
            FDPricingQuaData.AddHeader("TIMESTEPS");
            FDPricingQuaData.AddHeader("XSTEPS");
            FDPricingQuaData.AddHeader("YSTEPS");
            FDPricingQuaData.AddValue("TIMESTEPS", 100);
            FDPricingQuaData.AddValue("XSTEPS", 100);
            FDPricingQuaData.AddValue("YSTEPS", 100);
            Yum.app.Data(FDPricingQuaData, FD_PRICING_TIME_STEPS_ID, Yum.warning);

            // Classical OAS solver given a clean price i.e. only Rates Model is used and CDS curve is ignored even if supplied.
            // Model: Pick between a Rates Model + CDS curve or a full Rates + Credit Model
            // Setting CALCULATE OAS TO RISK FREE = TRUE drops the CDS curve/Credit model even if supplied and OAS, Expected Maturity, Option Value are all calculated using Rates model only
            string US48250NAB10_BOND_OAS_SOLVER_NO_CDS_ID = "US48250NAB10.BOND.OAS.SOLVER.NO.CDS";
            PricerOAS US48250NAB10_BOND_OAS_SOLVER_NO_CDS = new PricerOAS(US48250NAB10_BOND_OAS_SOLVER_NO_CDS_ID, TODAY_DATE, US48250NAB10_BOND_ID,
                                                                          FD_PRICING_TIME_STEPS_ID, "BackwardFinDiff",
                                                                          "CALLABLEBOND", YUM_HW1F_CR_DET_ID, "30/360", CORP_BOND_SETTLE_DT_ID,
                                                                          "S", Yum.app, Yum.warning, 1, 101.419, -1E-22, true);

            // OAS_W_O_CDS: OAS implied Opt value  from market price. NO CDS CURVE.  
            // From the OAS solver, a user can retrieve exercise probability(table) on each call date +expected call / maturity based on the probability weights of each call date.
            // BBG OAS are based on a CMT US Treasury curve. CMT curve is not set up in the xls but can be done. 
            // Please note that OAS and related metrics are solved by using the DOMESTIC YIELD CURVE input into the Rates Model
            // EXPECTED_MATURITY_W_O_CDS: THis expected call date will be used to calculated BCDS. This is derived from Rates model only

            string[] output_header_bond_oas_no_cds = { "OAS SPREAD", "OPT", "US48250NAB10.BOND.EXPECTEDMATURITY", "CALLABLEBOND.DIRTY", "CALLABLEBOND.CLEAN"};
            ApplicationData output_bond_oas_no_cds = Yum.app.View(US48250NAB10_BOND_OAS_SOLVER_NO_CDS_ID, output_header_bond_oas_no_cds, Yum.warning);

            // Output format
            Console.WriteLine(new string('=', 50));
            Console.WriteLine("OAS Solver NO CDS Tab Output");
            Console.WriteLine(new string('=', 50));
            Console.WriteLine("US48250NAB10.BOND.OAS.SOLVER.NO.CDS:");
            Console.WriteLine(output_bond_oas_no_cds);
            Console.WriteLine("");

            // OAS.NO.CDS.PRICER
            // OAS pricer with the solved OAS input to be fed into the rates risk report. 
            // The idea is to hold the solved OAS constant and bump the zero rates of the underlying rates model curve up and down. NO CDS CURVE.
            string US48250NAB10_BOND_NO_CDS_PRICER_ID = "US48250NAB10.BOND.OAS.NO.CDS.PRICER";           
            double US48250NAB10_BOND_NO_CDS_PRICER_SPREAD = Convert.ToDouble(output_bond_oas_no_cds.Data("OAS SPREAD")[0].ToString());
            PricerOAS US48250NAB10_BOND_NO_CDS_PRICER = new PricerOAS(US48250NAB10_BOND_NO_CDS_PRICER_ID, TODAY_DATE, US48250NAB10_BOND_ID,
                                                                      FD_PRICING_TIME_STEPS_ID, "BackwardFinDiff",
                                                                      "CALLABLEBOND", YUM_HW1F_CR_DET_ID, "30/360", CORP_BOND_SETTLE_DT_ID,
                                                                      "S", Yum.app, Yum.warning, 2, 0, 0, true, US48250NAB10_BOND_NO_CDS_PRICER_SPREAD);

            // EFFECTIVE_DURATION_REPORT_NO_CDS
            // Underlying Rates Curve has been moved up and down by 1 bp  while solved OAS is held constant.
            // Rates Model is not recalibrated upon moving the Zero Rates. NO CDS CURVE
            string US48250NAB10_BOND_EFFECTIVE_DURATION_REPORT_NO_CDS_ID = "US48250NAB10.BOND.EFFECTIVE_DURATION_REPORT_NO_CDS";
            ReportYield US48250NAB10_BOND_EFFECTIVE_DURATION_REPORT_NO_CDS = new ReportYield(US48250NAB10_BOND_EFFECTIVE_DURATION_REPORT_NO_CDS_ID,
                                                                                             US48250NAB10_BOND_NO_CDS_PRICER_ID, 0.0001, "ZERO RATE SHIFT", "ADDITIVE",
                                                                                             false, true, false, TODAY_ID, "S", Yum.app, Yum.warning);

            // Calculate NO_CDS_EFFECTIVE_DURATION and NO_CDS_EFFECTIVE_CONVEXITY
            string[] output_header_yield_report_no_cds = { "CALLABLEBOND.DELTA", "CALLABLEBOND.GAMMA" };
            ApplicationData output_yield_report_no_cds = Yum.app.View(US48250NAB10_BOND_EFFECTIVE_DURATION_REPORT_NO_CDS_ID, output_header_yield_report_no_cds, Yum.warning);

            double CALLABLEBOND_DELTA_NO_CDS = Convert.ToDouble(output_yield_report_no_cds.Data("CALLABLEBOND.DELTA")[0].ToString());
            double CALLABLEBOND_GAMMA_NO_CDS = Convert.ToDouble(output_yield_report_no_cds.Data("CALLABLEBOND.GAMMA")[0].ToString());
            double CALLABLEBOND_DIRTY_NO_CDS = Convert.ToDouble(output_bond_oas_no_cds.Data("CALLABLEBOND.DIRTY")[0].ToString());

            // OAS held constant while underlying Rates Curve is bumped up & down by amount in cell K8. You can shift either Zero Rate Shift OR Quoted Rate .
            // Because we DF based curve, we can only do Zero Rate Shift. 
            // When we build a curve using instruments we can do either. Model is not recalibrated. Risk report in column AK. CDS CURVE IS IGNORED 
            // (Up Price - Down Price)/ (2 * Shift Amount * 10000). It does not normalize by(-(1 / Price)).We need to externally normalize it by the Base Price
            double NO_CDS_EFFECTIVE_DURATION = -(CALLABLEBOND_DELTA_NO_CDS / CALLABLEBOND_DIRTY_NO_CDS) * 10000;
            double NO_CDS_EFFECTIVE_CONVEXITY = (CALLABLEBOND_GAMMA_NO_CDS / CALLABLEBOND_DIRTY_NO_CDS) * 10000 * 100;
            
            // Output
            Console.WriteLine("NO_CDS_EFFECTIVE_DURATION:         \t" + NO_CDS_EFFECTIVE_DURATION);
            Console.WriteLine("NO_CDS_EFFECTIVE_CONVEXITY:        \t" + NO_CDS_EFFECTIVE_CONVEXITY);
            Console.WriteLine("");

            // KEY_RATE_EFFECTIVE_DURATION_REPORT_NO_CDS
            // This is the Key Rate Duration report with Zero Rate Shift. No CDS Curve
            // Key maturities data object
            string KEY_MATURITIES_ID = "KEYMATURITIES_TABLE";

            ApplicationData KeyMaturitiesData = new ApplicationData();
            KeyMaturitiesData.AddHeader("KEY MATURITIES");
            KeyMaturitiesData.AddValues("KEY MATURITIES", DemoData.US48250NAB10_KEY_MATURITIES);
            Yum.app.Data(KeyMaturitiesData, KEY_MATURITIES_ID, Yum.warning);

            string US48250NAB10_BOND_KEY_EFFECTIVE_DURATION_REPORT_NO_CDS_ID = "US48250NAB10.BOND.KEY_RATE_EFFECTIVE_DURATION_REPORT_NO_CDS";
            ReportYield US48250NAB10_BOND_KEY_EFFECTIVE_DURATION_REPORT_NO_CDS = new ReportYield(US48250NAB10_BOND_KEY_EFFECTIVE_DURATION_REPORT_NO_CDS_ID,
                                                                                                 US48250NAB10_BOND_NO_CDS_PRICER_ID, 0.0001, "ZERO RATE SHIFT", "ADDITIVE",
                                                                                                 false, false, false, TODAY_ID, "S", Yum.app, Yum.warning, KEY_MATURITIES_ID);
            // Output key rate duration report
            string[] output_header_key_yield_report_no_cds = { "CURVE CURRENCY", "CURVE ID", "DATE", "TENOR", "CALLABLEBOND.DELTA",	"CALLABLEBOND.GAMMA" };
            ApplicationData output_yield_key_report_no_cds = Yum.app.View(US48250NAB10_BOND_KEY_EFFECTIVE_DURATION_REPORT_NO_CDS_ID, output_header_key_yield_report_no_cds, Yum.warning);
            
            // Output
            Console.WriteLine("US48250NAB10.BOND.KEY_RATE_EFFECTIVE_DURATION_REPORT_NO_CDS:");
            Console.WriteLine(output_yield_key_report_no_cds);
            Console.WriteLine("");

            // SPREAD_DV01_NO_CDS_UP_PRICER
            // Underlying Rates Curve has been held constant while OAS is moved up  by 1 bp . NO CDS CURVE
            string US48250NAB10_BOND_DV01_UP_NO_CDS_PRICER_ID = "US48250NAB10.BOND.SPREAD_DV01_NO_CDS_UP_PRICER";
            double US48250NAB10_BOND_DV01_UP_NO_CDS_PRICER_SPREAD = US48250NAB10_BOND_NO_CDS_PRICER_SPREAD + 0.0001;
            PricerOAS US48250NAB10_BOND_DV01_UP_NO_CDS_PRICER = new PricerOAS(US48250NAB10_BOND_DV01_UP_NO_CDS_PRICER_ID, TODAY_DATE, US48250NAB10_BOND_ID,
                                                                           FD_PRICING_TIME_STEPS_ID, "BackwardFinDiff",
                                                                           "CALLABLEBOND", YUM_HW1F_CR_DET_ID, "30/360", CORP_BOND_SETTLE_DT_ID,
                                                                           "S", Yum.app, Yum.warning, 2, 0, 0, true, US48250NAB10_BOND_DV01_UP_NO_CDS_PRICER_SPREAD);

            // SPREAD_DV01_NO_CDS_DOWN_PRICER
            // Underlying Rates Curve has been held constant while OAS is moved down  by 1 bp . NO CDS CURVE
            string US48250NAB10_BOND_DV01_DOWN_NO_CDS_PRICER_ID = "US48250NAB10.BOND.SPREAD_DV01_NO_CDS_DOWN_PRICER";
            double US48250NAB10_BOND_DV01_DOWN_NO_CDS_PRICER_SPREAD = US48250NAB10_BOND_NO_CDS_PRICER_SPREAD - 0.0001;
            PricerOAS US48250NAB10_BOND_DV01_DOWN_NO_CDS_PRICER = new PricerOAS(US48250NAB10_BOND_DV01_DOWN_NO_CDS_PRICER_ID, TODAY_DATE, US48250NAB10_BOND_ID,
                                                                           FD_PRICING_TIME_STEPS_ID, "BackwardFinDiff",
                                                                           "CALLABLEBOND", YUM_HW1F_CR_DET_ID, "30/360", CORP_BOND_SETTLE_DT_ID,
                                                                           "S", Yum.app, Yum.warning, 2, 0, 0, true, US48250NAB10_BOND_DV01_DOWN_NO_CDS_PRICER_SPREAD);

            // NO_CDS_SPREAD_DV01_DURATION
            // OAS is bumped up and down by 1 bp while holding Rates curve constant.  CDS CURVE IS IGNORED
            // (Up Price - Down Price)/ (2 * Shift Amount * 10000). It does not normalize by(-(1 / Price)).We need to externally normalize it by the Base Price
            string[] output_header_dv01_up_no_cds = { "CALLABLEBOND.CLEAN" };
            ApplicationData output_dv01_up_no_cds = Yum.app.View(US48250NAB10_BOND_DV01_UP_NO_CDS_PRICER_ID, output_header_dv01_up_no_cds, Yum.warning);
            ApplicationData output_dv01_down_no_cds = Yum.app.View(US48250NAB10_BOND_DV01_DOWN_NO_CDS_PRICER_ID, output_header_dv01_up_no_cds, Yum.warning);

            double CALLABLEBOND_CLEAN_UP_NO_CDS = Convert.ToDouble(output_dv01_up_no_cds.Data("CALLABLEBOND.CLEAN")[0].ToString());
            double CALLABLEBOND_CLEAN_DOWN_NO_CDS = Convert.ToDouble(output_dv01_down_no_cds.Data("CALLABLEBOND.CLEAN")[0].ToString());
            double CALLABLEBOND_CLEAN_NO_CDS = Convert.ToDouble(output_bond_oas_no_cds.Data("CALLABLEBOND.CLEAN")[0].ToString());

            double NO_CDS_SPREAD_DV01_DURATION = -(CALLABLEBOND_CLEAN_UP_NO_CDS - CALLABLEBOND_CLEAN_DOWN_NO_CDS) / (2 * 0.0001 * CALLABLEBOND_CLEAN_NO_CDS);

            // NO_CDS_SPREAD_DV01_CONVEXITY
            // OAS is bumped up and down by 1 bp while holding Rates curve constant.  CDS CURVE IS IGNORED
            double NO_CDS_SPREAD_DV01_CONVEXITY = ((CALLABLEBOND_CLEAN_UP_NO_CDS + CALLABLEBOND_CLEAN_DOWN_NO_CDS) - (2 * CALLABLEBOND_CLEAN_NO_CDS)) / (2 * 0.0001 * CALLABLEBOND_CLEAN_NO_CDS);
            
            // Output
            Console.WriteLine("NO_CDS_SPREAD_DV01_DURATION:         \t" + NO_CDS_SPREAD_DV01_DURATION);
            Console.WriteLine("NO_CDS_SPREAD_DV01_CONVEXITY:        \t" + NO_CDS_SPREAD_DV01_CONVEXITY);
            Console.WriteLine("");


            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // OAS Solver W CDS
            // Classical OAS solver given a clean price i.e. only Rates Model is used and CDS curve is ignored even if supplied.
            // Model: Pick between a Rates Model + CDS curve or a full Rates + Credit Model
            // Setting CALCULATE OAS TO RISK FREE = TRUE drops the CDS curve/Credit model even if supplied and OAS, Expected Maturity, Option Value are all calculated using Rates model only
            string US48250NAB10_BOND_OAS_SOLVER_CDS_ID = "US48250NAB10.BOND.OAS.SOLVER.CDS";
            PricerOAS US48250NAB10_BOND_OAS_SOLVER_CDS = new PricerOAS(US48250NAB10_BOND_OAS_SOLVER_CDS_ID, TODAY_DATE, US48250NAB10_BOND_ID,
                                                                          FD_PRICING_TIME_STEPS_ID, "BackwardFinDiff",
                                                                          "CALLABLEBOND", YUM_HW1F_CR_DET_ID, "30/360", CORP_BOND_SETTLE_DT_ID,
                                                                          "S", Yum.app, Yum.warning, 1, 101.419, -1E-22, false);

            // EXPECTED_MATURITY_W_CDS: THis expected call date will be used to calculated BCDS. This is derived from Rates model only

            string[] output_header_bond_oas_cds = { "OAS SPREAD", "OPT", "US48250NAB10.BOND.EXPECTEDMATURITY", "CALLABLEBOND.DIRTY", "CALLABLEBOND.CLEAN" };
            ApplicationData output_bond_oas_cds = Yum.app.View(US48250NAB10_BOND_OAS_SOLVER_CDS_ID, output_header_bond_oas_cds, Yum.warning);

            // Output format
            Console.WriteLine(new string('=', 50));
            Console.WriteLine("OAS Solver W CDS Tab Output");
            Console.WriteLine(new string('=', 50));
            Console.WriteLine("US48250NAB10.BOND.OAS.SOLVER.CDS:");
            Console.WriteLine("US48250NAB10.BOND.OAS.SOLVER.CDS Output:");
            Console.WriteLine(output_bond_oas_cds);
            Console.WriteLine("");

            // OAS.CDS.PRICER
            // OAS pricer with the solved OAS input to be fed into the rates risk report. 
            // The idea is to hold the solved OAS constant and bump the zero rates of the underlying rates model curve up and down. NO CDS CURVE.
            string US48250NAB10_BOND_CDS_PRICER_ID = "US48250NAB10.BOND.OAS.CDS.PRICER";
            double US48250NAB10_BOND_CDS_PRICER_SPREAD = Convert.ToDouble(output_bond_oas_cds.Data("OAS SPREAD")[0].ToString());
            PricerOAS US48250NAB10_BOND_CDS_PRICER = new PricerOAS(US48250NAB10_BOND_CDS_PRICER_ID, TODAY_DATE, US48250NAB10_BOND_ID,
                                                                      FD_PRICING_TIME_STEPS_ID, "BackwardFinDiff",
                                                                      "CALLABLEBOND", YUM_HW1F_CR_DET_ID, "30/360", CORP_BOND_SETTLE_DT_ID,
                                                                      "S", Yum.app, Yum.warning, 2, 0, 0, false, US48250NAB10_BOND_CDS_PRICER_SPREAD);

            // EFFECTIVE_DURATION_REPORT_CDS
            // Underlying Rates Curve has been moved up and down by 1 bp while solved OAS is held constant.
            // Rates Model is not recalibrated upon moving the Zero Rates. NO CDS CURVE
            string US48250NAB10_BOND_EFFECTIVE_DURATION_REPORT_W_CDS_ID = "US48250NAB10.BOND.EFFECTIVE_DURATION_REPORT_W_CDS";
            ReportYield US48250NAB10_BOND_EFFECTIVE_DURATION_REPORT_W_CDS = new ReportYield(US48250NAB10_BOND_EFFECTIVE_DURATION_REPORT_W_CDS_ID,
                                                                                            US48250NAB10_BOND_CDS_PRICER_ID, 0.0001, "ZERO RATE SHIFT", "ADDITIVE",
                                                                                            false, true, false, TODAY_ID, "S", Yum.app, Yum.warning);

            // Calculate W_CDS_EFFECTIVE_DURATION and W_CDS_EFFECTIVE_CONVEXITY
            string[] output_header_yield_report_w_cds = { "CALLABLEBOND.DELTA", "CALLABLEBOND.GAMMA" };
            ApplicationData output_yield_report_w_cds = Yum.app.View(US48250NAB10_BOND_EFFECTIVE_DURATION_REPORT_W_CDS_ID, output_header_yield_report_w_cds, Yum.warning);

            double CALLABLEBOND_DELTA_W_CDS = Convert.ToDouble(output_yield_report_w_cds.Data("CALLABLEBOND.DELTA")[0].ToString());
            double CALLABLEBOND_GAMMA_W_CDS = Convert.ToDouble(output_yield_report_w_cds.Data("CALLABLEBOND.GAMMA")[0].ToString());
            double CALLABLEBOND_DIRTY_W_CDS = Convert.ToDouble(output_bond_oas_cds.Data("CALLABLEBOND.DIRTY")[0].ToString());

            // OAS held constant while underlying Rates Curve is bumped up & down by amount in cell K8. You can shift either Zero Rate Shift OR Quoted Rate .
            // Because we DF based curve, we can only do Zero Rate Shift. 
            // When we build a curve using instruments we can do either. Model is not recalibrated. Risk report in column AK. CDS CURVE IS IGNORED 
            // (Up Price - Down Price)/ (2 * Shift Amount * 10000). It does not normalize by(-(1 / Price)).We need to externally normalize it by the Base Price
            double EFFECTIVE_DURATION_W_CDS = -(CALLABLEBOND_DELTA_W_CDS / CALLABLEBOND_DIRTY_W_CDS) * 10000;
            double EFFECTIVE_CONVEXITY_W_CDS = (CALLABLEBOND_GAMMA_W_CDS / CALLABLEBOND_DIRTY_W_CDS) * 10000 * 100;
            
            // Output
            Console.WriteLine("EFFECTIVE_DURATION_W_CDS:         \t" + EFFECTIVE_DURATION_W_CDS);
            Console.WriteLine("EFFECTIVE_CONVEXITY_W_CDS:        \t" + EFFECTIVE_CONVEXITY_W_CDS);
            Console.WriteLine("");

            // KEY_RATE_EFFECTIVE_DURATION_REPORT_W_CDS
            // This is the Key Rate Duration report with Zero Rate Shift. No CDS Curve
            string US48250NAB10_BOND_KEY_EFFECTIVE_DURATION_REPORT_W_CDS_ID = "US48250NAB10.BOND.KEY_RATE_EFFECTIVE_DURATION_REPORT_W_CDS";
            ReportYield US48250NAB10_BOND_KEY_EFFECTIVE_DURATION_REPORT_W_CDS = new ReportYield(US48250NAB10_BOND_KEY_EFFECTIVE_DURATION_REPORT_W_CDS_ID,
                                                                                                US48250NAB10_BOND_CDS_PRICER_ID, 0.0001, "ZERO RATE SHIFT", "ADDITIVE",
                                                                                                false, false, false, TODAY_ID, "S", Yum.app, Yum.warning, KEY_MATURITIES_ID);
            // Output key rate duration report
            string[] output_header_key_yield_report_w_cds = { "CURVE CURRENCY", "CURVE ID", "DATE", "TENOR", "CALLABLEBOND.DELTA", "CALLABLEBOND.GAMMA" };
            ApplicationData output_yield_key_report_w_cds = Yum.app.View(US48250NAB10_BOND_KEY_EFFECTIVE_DURATION_REPORT_W_CDS_ID, output_header_key_yield_report_w_cds, Yum.warning);

            // Output
            Console.WriteLine("US48250NAB10.BOND.KEY_RATE_EFFECTIVE_DURATION_REPORT_W_CDS:         \t");
            Console.WriteLine(output_yield_key_report_w_cds);
            Console.WriteLine("");

            // SPREAD_DV01_W_CDS_UP_PRICER
            // Underlying Rates Curve has been held constant while OAS is moved up  by 1 bp . NO CDS CURVE
            string US48250NAB10_BOND_DV01_UP_W_CDS_PRICER_ID = "US48250NAB10.BOND.SPREAD_DV01_W_CDS_UP_PRICER";
            double US48250NAB10_BOND_DV01_UP_W_CDS_PRICER_SPREAD = US48250NAB10_BOND_CDS_PRICER_SPREAD + 0.0001;
            PricerOAS US48250NAB10_BOND_DV01_W_NO_CDS_PRICER = new PricerOAS(US48250NAB10_BOND_DV01_UP_W_CDS_PRICER_ID, TODAY_DATE, US48250NAB10_BOND_ID,
                                                                             FD_PRICING_TIME_STEPS_ID, "BackwardFinDiff",
                                                                             "CALLABLEBOND", YUM_HW1F_CR_DET_ID, "30/360", CORP_BOND_SETTLE_DT_ID,
                                                                             "S", Yum.app, Yum.warning, 2, 0, 0, false, US48250NAB10_BOND_DV01_UP_W_CDS_PRICER_SPREAD);

            // SPREAD_DV01_W_CDS_DOWN_PRICER
            // Underlying Rates Curve has been held constant while OAS is moved down  by 1 bp . NO CDS CURVE
            string US48250NAB10_BOND_DV01_DOWN_W_CDS_PRICER_ID = "US48250NAB10.BOND.SPREAD_DV01_W_CDS_DOWN_PRICER";
            double US48250NAB10_BOND_DV01_DOWN_W_CDS_PRICER_SPREAD = US48250NAB10_BOND_CDS_PRICER_SPREAD - 0.0001;
            PricerOAS US48250NAB10_BOND_DV01_DOWN_W_CDS_PRICER = new PricerOAS(US48250NAB10_BOND_DV01_DOWN_W_CDS_PRICER_ID, TODAY_DATE, US48250NAB10_BOND_ID,
                                                                               FD_PRICING_TIME_STEPS_ID, "BackwardFinDiff",
                                                                               "CALLABLEBOND", YUM_HW1F_CR_DET_ID, "30/360", CORP_BOND_SETTLE_DT_ID,
                                                                               "S", Yum.app, Yum.warning, 2, 0, 0, false, US48250NAB10_BOND_DV01_DOWN_W_CDS_PRICER_SPREAD);

            // SPREAD_DV01_DURATION_W_CDS
            // OAS is bumped up and down by 1 bp while holding Rates curve constant.  CDS CURVE IS IGNORED
            // (Up Price - Down Price)/ (2 * Shift Amount * 10000). It does not normalize by(-(1 / Price)).We need to externally normalize it by the Base Price
            string[] output_header_dv01_up_w_cds = { "CALLABLEBOND.CLEAN" };
            ApplicationData output_dv01_up_w_cds = Yum.app.View(US48250NAB10_BOND_DV01_UP_W_CDS_PRICER_ID, output_header_dv01_up_w_cds, Yum.warning);
            ApplicationData output_dv01_down_w_cds = Yum.app.View(US48250NAB10_BOND_DV01_DOWN_W_CDS_PRICER_ID, output_header_dv01_up_w_cds, Yum.warning);

            double CALLABLEBOND_CLEAN_UP_W_CDS = Convert.ToDouble(output_dv01_up_w_cds.Data("CALLABLEBOND.CLEAN")[0].ToString());
            double CALLABLEBOND_CLEAN_DOWN_W_CDS = Convert.ToDouble(output_dv01_down_w_cds.Data("CALLABLEBOND.CLEAN")[0].ToString());
            double CALLABLEBOND_CLEAN_W_CDS = Convert.ToDouble(output_bond_oas_cds.Data("CALLABLEBOND.CLEAN")[0].ToString());

            double SPREAD_DV01_DURATION_W_CDS = -(CALLABLEBOND_CLEAN_UP_W_CDS - CALLABLEBOND_CLEAN_DOWN_W_CDS) / (2 * 0.0001 * CALLABLEBOND_CLEAN_W_CDS);

            // SPREAD_DV01_CONVEXITY_W_CDS
            // OAS is bumped up and down by 1 bp while holding Rates curve constant.  CDS CURVE IS IGNORED
            double SPREAD_DV01_CONVEXITY_W_CDS = ((CALLABLEBOND_CLEAN_UP_W_CDS + CALLABLEBOND_CLEAN_DOWN_W_CDS) - (2 * CALLABLEBOND_CLEAN_W_CDS)) / (2 * 0.0001 * CALLABLEBOND_CLEAN_W_CDS);

            // Output
            Console.WriteLine("SPREAD_DV01_DURATION_W_CDS:         \t" + SPREAD_DV01_DURATION_W_CDS);
            Console.WriteLine("SPREAD_DV01_CONVEXITY_W_CDS:        \t" + SPREAD_DV01_CONVEXITY_W_CDS);
            Console.WriteLine("");


            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // CR01
            // US48250NAB10_BOND_CDS_PRICER: This pricer holds the OAS solved for in presence of CDS curve constant. 
            // It also holds the Rates curve. So, we hold both OAS + Rates curve constant and bump the CDS curve only.
            // ADDITIVE: Can be Percentage as well. In case of additive it is absolute bps ( 1bp default) added to the CDS curve.
            // In case of Percentage, please enter the percentage amount to be shifted directly. For example 1%, 10% etc
            string US48250NAB10_CR01_REPORT_ID = "US48250NAB10.CR01.REPORT";
            ReportCredit US48250NAB10_CR01_REPORT = new ReportCredit(US48250NAB10_CR01_REPORT_ID, "SPREAD", US48250NAB10_BOND_CDS_PRICER_ID, false,
                                                                     Yum.app, Yum.warning, 0.0001, "ADDITIVE", true, false);

            // CR01_DURATION
            // CDS curve is bumped up and down while holding Rates curve and sovled for OAS (in presence of CDS curve). 
            // The callableBond.Delta = (Up Price - Down Price)/ (2 * Shift Amount * 10000). 
            // It does not normalize by(-(1 / Price)).We need to externally normalize it by the Base Price
            string[] output_header_cr01 = { "CALLABLEBOND.DELTA", "CALLABLEBOND.GAMMA" };
            ApplicationData output_cr01 = Yum.app.View(US48250NAB10_CR01_REPORT_ID, output_header_cr01, Yum.warning);

            double CALLABLEBOND_DELTA_CR01 = Convert.ToDouble(output_cr01.Data("CALLABLEBOND.DELTA")[0].ToString());
            double CR01_DURATION = -(CALLABLEBOND_DELTA_CR01 / CALLABLEBOND_DIRTY_W_CDS) * 10000;

            // Output format
            Console.WriteLine(new string('=', 50));
            Console.WriteLine("CR01 Tab Output");
            Console.WriteLine(new string('=', 50));
            Console.WriteLine("US48250NAB10.CR01.REPORT:         \t");
            Console.WriteLine(output_cr01);
            Console.WriteLine("");
            Console.WriteLine("CR01_DURATION:        \t" + CR01_DURATION);
            Console.WriteLine("");

            // CR10 REPORT: SHIFT 10BPS
            string US48250NAB10_CR10_REPORT_ID = "US48250NAB10.CR10.REPORT";
            ReportCredit US48250NAB10_CR10_REPORT = new ReportCredit(US48250NAB10_CR10_REPORT_ID, "SPREAD", US48250NAB10_BOND_CDS_PRICER_ID, false,
                                                                     Yum.app, Yum.warning, 0.001, "ADDITIVE", true, false);

            // CR10_DURATION
            string[] output_header_cr10 = { "CALLABLEBOND.DELTA", "CALLABLEBOND.GAMMA" };
            ApplicationData output_cr10 = Yum.app.View(US48250NAB10_CR10_REPORT_ID, output_header_cr10, Yum.warning);

            double CALLABLEBOND_DELTA_CR10 = Convert.ToDouble(output_cr10.Data("CALLABLEBOND.DELTA")[0].ToString());
            double CR10_DURATION = -(CALLABLEBOND_DELTA_CR10 / CALLABLEBOND_DIRTY_W_CDS) * 10000;

            // Output
            Console.WriteLine("US48250NAB10.CR10.REPORT:         \t");
            Console.WriteLine(output_cr10);
            Console.WriteLine("");
            Console.WriteLine("CR10_DURATION:        \t" + CR10_DURATION);
            Console.WriteLine("");

            // JUMP_TO_DEFAULT_REPORT
            // Jump to Default Risk Report where OAS and Rates curve are held constant, 
            // and the CDS curve is stressed to mimic a default based on user input date of when to begin default
            // Begin default date
            DateTime begin_default = Yum.app.AddTenor(TODAY_DATE, "1m", "F", "NXNY", Yum.warning);
            string US48250NAB10_JUMP_TO_DEFAULT_REPORT_ID = "US48250NAB10.JUMP_TO_DEFAULT_REPORT";
            ReportCredit US48250NAB10_JUMP_TO_DEFAULT_REPORT = new ReportCredit(US48250NAB10_JUMP_TO_DEFAULT_REPORT_ID, "START DEFAULT", US48250NAB10_BOND_CDS_PRICER_ID, false,
                                                                                Yum.app, Yum.warning, 0, "", false, false, begin_default);

            string[] output_header_cr_default = { "CALLABLEBOND.CHANGE" };
            ApplicationData output_cr_default = Yum.app.View(US48250NAB10_JUMP_TO_DEFAULT_REPORT_ID, output_header_cr_default, Yum.warning);

            // Output
            Console.WriteLine("US48250NAB10.JUMP_TO_DEFAULT_REPORT:         \t");
            Console.WriteLine(output_cr_default);
            Console.WriteLine("");


            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // BCDS
            // OAS.MATURITY.BOND
            // New Bullet Bond with Maturity = Expected Maturity (from "OAS Solver No CDS" part) derived via OAS (with no CDS) solver. 
            // Input Price = Today's Callable Bond Price. This bond will be used to strip the Surv Prob Curve.  
            string OAS_MATURITY_BOND_ID = "OAS.MATURITY.BOND";
            DateTime OAS_MATURITY = DateTime.Parse(output_bond_oas_no_cds.Data("US48250NAB10.BOND.EXPECTEDMATURITY")[0].ToString());

            BondInst OAS_MATURITY_BOND = new BondInst(OAS_MATURITY_BOND_ID, "USD", OAS_MATURITY, 0.0525, "NX.CONV.IR.BOND.US.CORPORATE",
                                                      US48250NAB10_accrual_start, 100, default(DateTime), default(DateTime),
                                                      Yum.app, Yum.warning, "6M", "", "", "", false, 0.4, 101.419, false, CORP_BOND_SETTLE_DT_ID, false);

            // EXPECTED_MATURITY_BOND_SP_CURVE
            // Surv Prob Curve stripping using truncated bond maturity (Expected Maturity) & input Market price to solve for BCDS. Truncated bond instrument is built above.
            // Good to use the same discounting curve as the model used to back out the expected maturity.i.e.the domestic yield curve supplied to the rates model
            // Bond instrument collection object            
            string US48250NAB10_EXPECTEDMAT_COLL_ID = "US48250NAB10.EXPECTEDMAT.COLL";
            InstColl US48250NAB10_EXPECTEDMAT_COLL = new InstColl(US48250NAB10_EXPECTEDMAT_COLL_ID, "USD", "", Yum.app, Yum.warning, OAS_MATURITY_BOND_ID);

            // USD_OIS_ID: Same curve as the Rates model curve used to calculate expected maturity without CDS curve
            string US48250NAB10_EXPECTED_MATURITY_BOND_SP_CURVE_ID = "US48250NAB10.EXPECTED_MATURITY_BOND_SP_CURVE";
            CreditObject US48250NAB10_EXPECTED_MATURITY_BOND_SP_CURVE = new CreditObject(US48250NAB10_EXPECTED_MATURITY_BOND_SP_CURVE_ID, "LINEAR", "LOGSP", TODAY_ID, "USD", US48250NAB10_EXPECTEDMAT_COLL_ID,
                                                                                         USD_OIS_ID, "ACT/360", "BOUND", Yum.app, Yum.warning, "6M");

            // OAS.MATURITY.CDS
            // New CDS instrument with maturity =  Expected Maturity (in the "OAS Solver NO CDS" part) derived via OAS (with no CDS) solver.
            // We want to calculate BCDS using the Surv Prob curve stripped from the expected maturity based bond instrument above
            string US48250NAB10_OA_MATURITY_CDS_ID = "US48250NAB10.OAS.MATURITY.CDS";
            CDSInst US48250NAB10_OA_MATURITY_CDS = new CDSInst(US48250NAB10_OA_MATURITY_CDS_ID, "USD", 0.4, "", US48250NAB10_accrual_start, false, true, 0, "", 
                                                               Yum.app, Yum.warning, 0, "", "", OAS_MATURITY, "NX.CONV.IR.BOND.US.CORPORATE");

            // BCDS.IMPLIED.CDS.PRICER
            // Good to use the same discounting curve as the model used to back out the expected maturity  & also the same discounting curve used to strip Surv Prob using the expected maturity based bond for BCDS curve stripping. 
            // This is to ensure our use of curves is consistent
            string US48250NAB10_BCDS_IMPLIED_CDS_PRICER_ID = "US48250NAB10.BCDS.IMPLIED.CDS.PRICER";
            AnalyticCDS US48250NAB10_BCDS_IMPLIED_CDS_PRICER = new AnalyticCDS(US48250NAB10_BCDS_IMPLIED_CDS_PRICER_ID, TODAY_DATE, US48250NAB10_OA_MATURITY_CDS_ID, US48250NAB10_EXPECTED_MATURITY_BOND_SP_CURVE_ID, 
                                                                               USD_OIS_ID, Yum.app, Yum.warning, "", "", "", "6M");

            // CDS PRICER BCDS
            string[] output_header_bcds = { "FORWARD" };
            ApplicationData output_bcds = Yum.app.View(US48250NAB10_BCDS_IMPLIED_CDS_PRICER_ID, output_header_bcds, Yum.warning);
            
            double US48250NAB10_BCDS = Convert.ToDouble(output_bcds.Data("FORWARD")[0].ToString());

            // INTERPOLATED CDS SPREAD
            // Interpolated from Yum CDS curve
            double INTERPOLATED_CDS_SPREAD = Yum.app.GetInterpolatedValue(YUM_CDS_INTERP_ID, OAS_MATURITY, Yum.warning);
            Console.WriteLine(new string('=', 50));
            Console.WriteLine("BCDS Tab Output");
            Console.WriteLine(new string('=', 50));
            Console.WriteLine("BCDS:                                  \t" + US48250NAB10_BCDS);
            Console.WriteLine("INTERPOLATED CDS SPREAD:               \t" + INTERPOLATED_CDS_SPREAD);
            Console.ReadKey();
            
            //ApplicationData output = Yum.app.View(US48250NAB10_OA_MATURITY_CDS_ID, Yum.warning);
            //Console.WriteLine(output);
            //XMLUtils.WriteXML(Yum.app, "C:/NxData/OneDrive - Numerix/Nx_xkong/Pre-sales/PolarAsset/PAM_YumPAMP/YumPAMP/DEMO.XML", Yum.warning);
        }
    }
}
